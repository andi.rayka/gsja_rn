import React, {useState} from 'react';

// Component
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  Picker,
  StyleSheet,
  Modal,
  ScrollView,
  Dimensions,
} from 'react-native';

// Micro Component
import Icon from 'react-native-vector-icons/FontAwesome5';
import warna from 'gsja/src/assets/colors';
import rowStyles from 'gsja/src/assets/styles';

const {spaceBetweenRow, flexStartRow} = rowStyles;

// Function
import DatePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

// Header Tiruan untuk modal.
// Default height untuk android adalah 56
const HeaderTiruan = ({title, onClose}) => {
  return (
    <View style={[flexStartRow, localStyles.containerHeaderTiruan]}>
      {/* Button Back */}
      <TouchableOpacity onPress={onClose}>
        <Icon name="arrow-left" size={23} color={warna.putihAsli} />
      </TouchableOpacity>
      {/* Text judul modal */}
      <Text
        style={{
          fontFamily: 'Roboto-Regular',
          fontSize: 22,
          color: warna.putihAsli,
          marginLeft: 17,
        }}>
        {title}
      </Text>
    </View>
  );
};

// Component Big List
export const BigList = ({label, listOption, selectedList, saveChange}) => {
  const [isListOpen, toggleOpenList] = useState(false);

  return (
    <View style={{flex: 1}}>
      {/* Label  */}
      <Text style={localStyles.inputLabel}>{label}</Text>

      {/* Button Open List */}
      <TouchableOpacity
        onPress={() => toggleOpenList(true)}
        style={[
          {
            padding: 5,
            marginBottom: 5,
            flex: 1,
            borderBottomColor: warna.abuabu2,
            borderBottomWidth: 1,
          },
          spaceBetweenRow,
        ]}>
        <Text
          style={{
            fontSize: 17,
            color: warna.hitamText,
            fontFamily: 'Roboto-Medium',
          }}>
          {selectedList.nameprovince
            ? selectedList.nameprovince
            : `Pilih ${label}`}
        </Text>
        <Icon name="list-ul" size={18} />
      </TouchableOpacity>

      {/* Modal List */}
      <Modal
        animationType="fade"
        transparent
        visible={isListOpen}
        onRequestClose={() => toggleOpenList(false)}>
        <HeaderTiruan
          title={`Pilih ${label}`}
          onClose={() => toggleOpenList(false)}
        />

        <ScrollView
          style={{
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            backgroundColor: warna.putihAsli,
          }}>
          {listOption.map((item, key) => {
            return (
              <TouchableOpacity
                key={key}
                onPress={() => {
                  toggleOpenList(false);
                  saveChange({
                    idprovince: item.idprovince,
                    nameprovince: item.nameprovince,
                  });
                }}
                style={localStyles.buttonListOption}>
                <Text>{item.nameprovince}</Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </Modal>
    </View>
  );
};

// Component Input Text
export const InputText = ({label, value, onChange}) => {
  return (
    <View style={{flex: 1}}>
      {/* Label  */}
      <Text style={localStyles.inputLabel}>{label}</Text>

      {/* Input Text  */}
      <TextInput
        style={localStyles.textinputStyles}
        underlineColorAndroid={warna.abuabu2}
        autoCapitalize="none"
        returnKeyType="next"
        value={value}
        onChangeText={onChange}
      />
    </View>
  );
};

// Component Input Text
export const InputNumber = ({label, value, onChange}) => {
  return (
    <View style={{flex: 1}}>
      {/* Label  */}
      <Text style={localStyles.inputLabel}>{label}</Text>

      {/* Input Text  */}
      <TextInput
        style={localStyles.textinputStyles}
        underlineColorAndroid={warna.abuabu2}
        autoCapitalize="none"
        returnKeyType="next"
        value={value}
        onChangeText={onChange}
        keyboardType="number-pad"
      />
    </View>
  );
};

// Component Input Multiline Text
export const InputMultiLine = ({label, value, onChange}) => {
  return (
    <View style={{flex: 1}}>
      {/* Label  */}
      <Text style={localStyles.inputLabel}>{label}</Text>

      {/* Input Text  */}
      <TextInput
        multiline
        style={[
          localStyles.textinputStyles,
          {
            borderColor: warna.abuabu2,
            borderWidth: 1,
            borderRadius: 3,
            marginTop: 10,
            marginBottom: 30,
          },
        ]}
        autoCapitalize="none"
        returnKeyType="next"
        value={value}
        onChangeText={onChange}
        numberOfLines={3}
        textAlignVertical="top"
      />
    </View>
  );
};

// Component Input dengan Dropdown
export const DropdownInput = ({
  label,
  selectedValue,
  onValueChange,
  arrayItem,
}) => {
  return (
    <View style={{flex: 1}}>
      {/* Label  */}
      <Text style={localStyles.inputLabel}>{label}</Text>
      <Picker
        selectedValue={selectedValue ? selectedValue : ''}
        onValueChange={onValueChange}>
        <Picker.Item label={`Pilih ${label}`} value="" />
        {arrayItem.map((item, key) => {
          return (
            <Picker.Item
              key={key}
              label={item[0]}
              value={item[1]}
              style={{flex: 1, width: '100%'}}
            />
          );
        })}
      </Picker>
    </View>
  );
};

// Component Input Tanggal
export const DateInput = ({label, selectedDate, onSelect}) => {
  const [isShowDate, toggleShowDate] = useState(false);

  const handleDate = (event, pickedDate) => {
    toggleShowDate(false);

    if (pickedDate) {
      const finalDate = moment(pickedDate).format('YYYY-MM-DD');

      onSelect(finalDate);
    }
  };

  return (
    <View style={{flex: 1}}>
      {/* Label  */}
      <Text style={localStyles.inputLabel}>{label}</Text>

      {/* Button Date Input  */}
      <TouchableOpacity
        onPress={() => toggleShowDate(true)}
        style={[
          {
            padding: 5,
            marginBottom: 5,
            flex: 1,
            borderBottomColor: warna.abuabu2,
            borderBottomWidth: 1,
          },
          spaceBetweenRow,
        ]}>
        <Text
          style={{
            fontSize: 17,
            color: warna.hitamText,
            fontFamily: 'Roboto-Medium',
          }}>
          {moment(selectedDate).format('DD MMMM YYYY')}
        </Text>
        <Icon name="calendar-alt" size={18} />
      </TouchableOpacity>

      {isShowDate && (
        <DatePicker
          mode="date"
          value={moment(selectedDate, 'YYYY-MM-DD').toDate()}
          onChange={handleDate}
        />
      )}
    </View>
  );
};

// Component Pilih apakah form adalah kota atau non-kota
export const ChooseIsKota = ({stateName, setStateName}) => {
  const [isKota, toggleIsKota] = useState(stateName.iskota == 0 ? false : true);

  const styles = StyleSheet.create({
    activeStyles: {
      width: 15,
      height: 15,
      borderWidth: 4,
      borderColor: warna.merahHeader,
      elevation: 3,
    },
    inactiveStyles: {
      width: 13,
      height: 13,
      borderWidth: 1,
      borderColor: warna.hitamText,
    },
  });
  const {activeStyles, inactiveStyles} = styles;

  // Button pilihan kota atau non-kota
  const ButtonChooseKota = ({onChoose, text, isActive}) => {
    return (
      <TouchableOpacity
        onPress={onChoose}
        style={[flexStartRow, {paddingVertical: 10, paddingRight: 10}]}>
        <View
          style={[
            isActive ? activeStyles : inactiveStyles,
            {
              borderRadius: 30,
              backgroundColor: warna.putihAsli,
              marginRight: 8,
            },
          ]}
        />
        <Text
          style={{
            fontSize: 16,
            color: warna.hitamText,
            fontFamily: 'Roboto-Medium',
          }}>
          {text}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <View>
      {/* Dua Button untuk memilih kota/non */}
      <View style={flexStartRow}>
        <ButtonChooseKota
          text="Kota"
          isActive={isKota}
          onChoose={() => {
            toggleIsKota(true);
            setStateName({...stateName, iskota: 1});
          }}
        />
        <ButtonChooseKota
          text="Non-Kota"
          isActive={!isKota}
          onChoose={() => {
            toggleIsKota(false);
            setStateName({...stateName, iskota: 0});
          }}
        />
      </View>

      {/* Menampilkan form yang berbeda untuk kota dan non-kota */}
      {isKota ? (
        <View>
          <InputText
            label="Kelurahan"
            value={stateName.kelurahan}
            onChange={newText =>
              setStateName({...stateName, kelurahan: newText})
            }
          />

          <InputText
            label="Kecamatan"
            value={stateName.kecamatan}
            onChange={newText =>
              setStateName({...stateName, kecamatan: newText})
            }
          />
          <InputText
            label="Kota"
            value={stateName.kota}
            onChange={newText => setStateName({...stateName, kota: newText})}
          />
        </View>
      ) : (
        <View>
          <InputText
            label="Desa"
            value={stateName.desa}
            onChange={newText => setStateName({...stateName, desa: newText})}
          />

          <InputText
            label="Kecamatan"
            value={stateName.kecamatan}
            onChange={newText =>
              setStateName({...stateName, kecamatan: newText})
            }
          />
          <InputText
            label="Kabupaten"
            value={stateName.kabupaten}
            onChange={newText =>
              setStateName({...stateName, kabupaten: newText})
            }
          />
        </View>
      )}
    </View>
  );
};

// Component Button Pilih Provinsi

// Local styles
const localStyles = StyleSheet.create({
  containerHeaderTiruan: {
    width: '100%',
    height: 56,
    backgroundColor: warna.merahHeader,
    elevation: 1,
    paddingHorizontal: 10,
  },
  buttonListOption: {
    backgroundColor: warna.putihAsli,
    paddingTop: 15,
    paddingBottom: 17,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: warna.abuabu1,
    borderRadius: 12,
  },
  textinputStyles: {
    paddingTop: 3,
    fontSize: 17,
    color: warna.hitamText,
    fontFamily: 'Roboto-Medium',
  },
  inputLabel: {
    fontSize: 14,
    color: warna.merahStatusbar,
    fontFamily: 'Roboto-Medium',
  },
});
