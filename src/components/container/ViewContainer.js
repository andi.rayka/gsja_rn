import React from 'react';
// Component
import {View, StatusBar} from 'react-native';
import LoadingScreen from 'gsja/src/components/common/LoadingScreen';

const ListContainer = ({children, isLoading, loadingScreenException}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#e9e9e9',
        paddingHorizontal: 9,
      }}>
      <StatusBar backgroundColor="#5e0101" />

      {isLoading ? <LoadingScreen /> : children}

      {loadingScreenException}
    </View>
  );
};

export default ListContainer;
