import React from 'react';
// Component
import {View, StatusBar, ScrollView} from 'react-native';
import LoadingScreen from 'gsja/src/components/common/LoadingScreen';

const ScrollviewContainer = ({
  isLoading,
  children,
  loadingScreenException,
  noPaddingHorizontal,
}) => {
  return (
    <ScrollView
      contentContainerStyle={{
        backgroundColor: '#e9e9e9',
        padding: 9,
        flexGrow: 1,
      }}>
      <StatusBar backgroundColor="#5e0101" />

      <View
        style={{
          flex: 1,
          backgroundColor: '#fff',
          paddingHorizontal: noPaddingHorizontal ? 0 : 11,
          paddingTop: 14,
          paddingBottom: 18,
        }}>
        {isLoading ? <LoadingScreen /> : children}

        {loadingScreenException}
      </View>
    </ScrollView>
  );
};

export default ScrollviewContainer;
