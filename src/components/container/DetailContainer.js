import React from 'react';

// Component
import {ScrollView, StatusBar, View, Text, StyleSheet} from 'react-native';
import LoadingScreen from 'gsja/src/components/common/LoadingScreen';
import {Image} from 'gsja/src/components/common';
import {Table, Row, Rows} from 'react-native-table-component';

// Micro Component
import warna from 'gsja/src/assets/colors';
import rowStyles from 'gsja/src/assets/styles';

const {spaceBetweenRow} = rowStyles;

const DetailContainer = ({
  // Array data yang ditampilkan di kiri dan kanan seprti tabel
  detailData,
  isLoading,
  imageURL,
  // Nama pemilik profil
  detailName,

  // Misalnya button
  komponenDiBawah,

  // Container tambahan di bawah, untuk tabel dan data lain
  containerTambahan,
}) => {
  return (
    <ScrollView
      contentContainerStyle={{
        backgroundColor: warna.abuabu1,
        padding: 9,
        flexGrow: 1,
      }}>
      <StatusBar backgroundColor={warna.merahStatusbar} />

      {isLoading ? (
        <LoadingScreen />
      ) : (
        //  Container Semua Konten
        <>
          <View
            style={{
              flex: 1,
              backgroundColor: warna.putihAsli,
              padding: 24,
              paddingBottom: 30,
            }}>
            {/* Foto Detail  */}
            <View
              style={{
                borderRadius: 160,
                overflow: 'hidden',
                width: 160,
                height: 160,
                alignSelf: 'center',
              }}>
              <Image
                source={{uri: imageURL}}
                style={{width: 160, height: 160, borderRadius: 160}}
              />
            </View>

            {/* Nama Detail  */}
            <Text
              style={{
                color: warna.hitamText,
                fontSize: 20,
                fontFamily: 'Roboto-Medium',
                textAlign: 'center',
                marginTop: 13,
              }}>
              {detailName}
            </Text>

            {/* Detail Data */}
            <View style={{marginTop: 20, marginBottom: 25}}>
              {detailData.map((item, key) => {
                return <BarisDetail key={key} kiri={item[0]} kanan={item[1]} />;
              })}
            </View>

            {komponenDiBawah}
          </View>

          {containerTambahan && (
            <View
              style={{
                lex: 1,
                backgroundColor: warna.putihAsli,
                paddingHorizontal: 16,
                paddingBottom: 19,
                borderTopColor: warna.abuabu1,
                borderTopWidth: 6,
              }}>
              {containerTambahan}
            </View>
          )}
        </>
      )}
    </ScrollView>
  );
};

// Contoh data yang dipakai
let tableHead = ['Head', 'Head2', 'Head3', 'Head4'];
let tableData = [
  ['1', '2', '3', '4'],
  ['a', 'b', 'c', 'd'],
  ['1', '2', '3', '456'],
  ['a', 'b', 'c', 'd'],
];

// Untuk table panjang, misalnya jenjang di edit profil
export const TableDataTambahan = ({
  judul,

  // lebar tiap kolom
  widthArrPerColumn,

  // Data header
  headerData,

  // untuk data di table
  contentData,
}) => {
  return (
    <>
      <Text
        style={{
          color: warna.hitamText,
          fontSize: 18,
          fontFamily: 'Roboto-Medium',
          marginTop: 20,
          marginBottom: 5,
        }}>
        {judul}
      </Text>
      <ScrollView horizontal>
        <Table>
          <Row
            data={headerData}
            style={styles.tableHead}
            textStyle={styles.textTableHeader}
            widthArr={widthArrPerColumn}
          />

          {contentData.length === 0 ? (
            <Text style={[styles.textTableContent, {textAlign: 'center'}]}>
              Belum ada data
            </Text>
          ) : (
            <Rows
              data={contentData}
              style={styles.tableContent}
              textStyle={[styles.textTableContent, {textAlign: 'center'}]}
              widthArr={widthArrPerColumn}
            />
          )}
        </Table>
      </ScrollView>
    </>
  );
};

// Table yang hanya isi dua kolom
export const TabelDataTwoRows = ({
  judul,

  leftHead,
  rightHead,

  contentData,
  leftKeyname,
  rightKeyname,
}) => {
  return (
    <>
      <Text
        style={{
          color: warna.hitamText,
          fontSize: 18,
          fontFamily: 'Roboto-Medium',
          marginTop: 20,
          marginBottom: 5,
        }}>
        {judul}
      </Text>

      {/* Table Header */}
      <View
        style={[
          spaceBetweenRow,
          {
            backgroundColor: '#E57373',
            borderRadius: 3,
            paddingVertical: 5,
            paddingHorizontal: 3,
          },
        ]}>
        <Text style={styles.textTableHeader}>{leftHead}</Text>
        <Text style={styles.textTableHeader}>{rightHead}</Text>
      </View>

      {/* Table Content */}
      {contentData.length === 0 ? (
        <Text style={[styles.textTableContent, {textAlign: 'center'}]}>
          Belum ada data
        </Text>
      ) : (
        contentData &&
        contentData.map((item, key) => {
          return (
            <View
              key={key}
              style={[
                spaceBetweenRow,
                {
                  borderRadius: 3,
                  paddingVertical: 5,
                  paddingHorizontal: 4,
                  elevation: 0.5,
                },
              ]}>
              <Text style={styles.textTableContent}>{item[leftKeyname]}</Text>
              <Text style={[styles.textTableContent, {textAlign: 'center'}]}>
                {item[rightKeyname]}
              </Text>
            </View>
          );
        })
      )}
    </>
  );
};

const BarisDetail = ({kiri, kanan}) => {
  return (
    <View style={[spaceBetweenRow, {marginTop: 8}]}>
      {/* Kolom Kiri */}
      <Text
        style={{
          flex: 4,
          color: warna.hitamText,
          fontSize: 14,
          fontFamily: 'Roboto-Regular',
        }}>
        {kiri}
      </Text>

      <Text
        style={{
          color: warna.hitamText,
          fontSize: 14,
          fontFamily: 'Roboto-Medium',
        }}>
        :{'  '}
      </Text>

      {/* Kolom Kanan */}
      <Text
        style={{
          flex: 5,
          color: warna.hitamText,
          fontSize: 14,
          fontFamily: 'Roboto-Medium',
        }}>
        {kanan}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  tableHead: {
    backgroundColor: '#E57373',
    borderRadius: 3,
    paddingVertical: 5,
    paddingHorizontal: 3,
  },
  tableContent: {
    borderRadius: 3,
    paddingVertical: 5,
    paddingHorizontal: 4,
    elevation: 0.5,
  },
  textTableHeader: {
    flex: 1,
    textAlign: 'center',
    color: '#fff',
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
  textTableContent: {
    flex: 1,
    color: '#505050',
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
});

export default DetailContainer;
