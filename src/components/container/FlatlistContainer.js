import React, {useState, useEffect} from 'react';

// Component
import {View, FlatList, ActivityIndicator, StatusBar, Text} from 'react-native';
import LoadingScreen from 'gsja/src/components/common/LoadingScreen';

// Micro Component
import warna from 'gsja/src/assets/colors';

// Function
import {apiPost} from 'gsja/src/services/api';

const ListContainer = ({
  // Data untuk panggil api
  url,
  additionalPostData,

  // Bagian dari flatlist
  renderItem,
  separatorItem,

  // Komponen selain flatlist
  loadingScreenException,
}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [fetchedData, setFetchedData] = useState([]);
  const [jumlahData, setJumlahData] = useState(0);
  const [limit, setlimit] = useState(10);

  useEffect(() => {
    // Data untuk filter list, dari additionalPostData
    let data = Object.assign({limit, offset: 0}, additionalPostData);

    // console.log('url', url);
    // console.log('Req List Data:', data);
    // console.log();

    let postData = [{url, data}];

    apiPost(postData)
      .then(resp => {
        const {list, jmldata} = resp[0];

        setFetchedData(list);
        setJumlahData(jmldata);
      })
      .then(() => {
        setIsRefreshing(false);
        setIsLoadMore(false);
        setIsLoading(false);
      });
  }, [url, additionalPostData, limit, isRefreshing]);

  // Bagian paling atas list, untuk semacam marginTop list
  const headerItem = () => (
    <View
      style={{
        backgroundColor: warna.abuabu1,
        height: 11,
        width: '100%',
      }}
    />
  );

  // Ada di paling bawah list
  const footerItem = () => {
    if (!isLoadMore) {
      return (
        <View
          style={{backgroundColor: warna.abuabu1, height: 11, width: '100%'}}
        />
      );
    }

    return (
      <ActivityIndicator animating size="large" color={warna.merahHeader} />
    );
  };

  // Jika loading, tampilkan loading screen
  if (isLoading) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: warna.abuabu1,
          paddingHorizontal: 9,
        }}>
        <StatusBar backgroundColor={warna.merahStatusbar} />
        <LoadingScreen />
      </View>
    );
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: warna.abuabu1,
        paddingHorizontal: 9,
      }}>
      <StatusBar backgroundColor={warna.merahStatusbar} />

      {fetchedData.length > 0 ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={fetchedData}
          renderItem={renderItem}
          keyExtractor={(item, key) => key.toString()}
          ItemSeparatorComponent={separatorItem}
          ListHeaderComponent={headerItem}
          ListFooterComponent={footerItem}
          refreshing={isRefreshing}
          onRefresh={() => {
            setlimit(10);
            setIsRefreshing(true);
          }}
          onEndReached={() => {
            // Jika sudah sampai jumlah data, maka tidak load more lagi
            if (limit >= jumlahData) {
              return null;
            }

            setlimit(limit + 10);
            setIsLoadMore(true);
          }}
          onEndReachedThreshold={0.5}
        />
      ) : (
        <Text
          style={{
            color: warna.hitamText,
            fontSize: 25,
            fontFamily: 'Roboto-Medium',
            marginTop: 15,
            marginHorizontal: 10,
            textAlign: 'center',
            flex: 1,
          }}>
          Tidak Ada Data
        </Text>
      )}

      {loadingScreenException}
    </View>
  );
};

export default ListContainer;
