import React from 'react';
import {TouchableOpacity, Text} from 'react-native';

import warna from 'gsja/src/assets/colors';

// Tombol Edit di paling bawah
const ButtonEdit = ({onPress, text}) => (
  <TouchableOpacity
    onPress={onPress}
    style={{
      backgroundColor: warna.merahHeader,
      paddingVertical: 13,
      marginHorizontal: 10,
      borderRadius: 5,
    }}>
    <Text
      style={{
        fontSize: 16,
        color: warna.putihAsli,
        fontFamily: 'Roboto-Medium',
        textAlign: 'center',
      }}>
      {text}
    </Text>
  </TouchableOpacity>
);

export default ButtonEdit;
