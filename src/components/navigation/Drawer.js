import React, {useEffect, useState} from 'react';

// Component
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {Image} from 'gsja/src/components/common';

// Micro Component
import IconAwesome from 'react-native-vector-icons/FontAwesome5';
import {DrawerItems} from 'react-navigation-drawer';
import strings from 'gsja/src/assets/strings';

const {drawer} = strings;

// Function
import {apiPost} from 'gsja/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import {setAsyncStorage} from '../../commonFunctions';

const Drawer = props => {
  const [userData, setUserData] = useState({});
  const [isMultipleGereja, setIsMultipleGereja] = useState(false);

  const {commonRow} = styles;

  // Ambil User Data dari Async Storage
  useEffect(() => {
    // console.log('drawer jalan');

    const getUserData = async () => {
      try {
        const data = await AsyncStorage.getItem('userData');

        let parsedData = JSON.parse(data);

        // Karena gambar kalau linknya sama, nggak akan ganti.
        // Makanya diakali pakai param waktu untuk lihat gambar
        // Biar tiap buka refresh
        const waktu = moment().unix();
        parsedData.foto += `?waktuInsert=${waktu}`;

        setUserData(parsedData);

        // console.log(props.navigation);
      } catch (error) {
        console.log(error.message);
      }
    };

    getUserData();
  }, [props.navigation.openDrawer]);

  useEffect(() => {
    if (userData.iduser) {
      const postData = [
        {
          url: '/getListProfileGereja',
          data: {
            id_pendeta: userData.iduser,
          },
        },
      ];

      apiPost(postData).then(([res]) => {
        if (res && res.length > 1) {
          setAsyncStorage('listGereja', JSON.stringify(res));
          setIsMultipleGereja(true);
        }
      });
    }
  }, [userData]);

  // Logout, menghapus semua data async storage dan menuju login
  const logout = async () => {
    try {
      await AsyncStorage.clear();
    } catch (error) {
      alert('Gagal logout');
    }

    props.navigation.navigate('stackAuth');
  };

  // console.log('foto di drawer: ', userData.foto);

  return (
    <View
      style={{
        flex: 1,
        paddingHorizontal: 14,
        paddingTop: 34,
        paddingBottom: 25,
      }}>
      {/* Container konten */}
      <ScrollView contentContainerStyle={{alignItems: 'center'}}>
        {/* Foto Profile User */}
        <View
          style={{
            borderRadius: 100,
            overflow: 'hidden',
            width: 100,
            height: 100,
            alignSelf: 'center',
          }}>
          <Image
            source={{uri: userData.foto}}
            style={{width: 100, height: 100, borderRadius: 100}}
          />
        </View>

        {/* Nama user */}
        <Text
          style={{
            fontSize: 20,
            fontFamily: 'Roboto-Medium',
            color: '#303030',
            marginTop: 17,
          }}>
          {userData.nama}
        </Text>

        {/* Nama hak akses dan unit user */}
        <Text
          style={{
            fontSize: 14,
            fontFamily: 'Roboto-Regular',
            color: '#303030',
            marginTop: 5,
          }}>
          {`${userData.namajabatan} - ${userData.namaunit}`}
        </Text>

        {/* Container Edit Profil dan Gereja  */}
        <View style={commonRow}>
          {/* Button edit profile */}
          <TouchableOpacity
            onPress={() => props.navigation.navigate('drawerEditProfil')}>
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'Roboto-Medium',
                color: '#b70000',
                marginTop: 7,
              }}>
              {drawer.label.editProfil}
            </Text>
          </TouchableOpacity>

          {userData.isgembala === 1 && (
            <>
              <View
                style={{
                  width: 2,
                  height: 18,
                  backgroundColor: '#707070',
                  marginTop: 5,
                  marginHorizontal: 12,
                }}
              />

              {/* Button edit Gereja */}
              <TouchableOpacity
                onPress={() => {
                  if (isMultipleGereja) {
                    props.navigation.navigate('PilihGereja');
                  } else {
                    props.navigation.navigate('DetailProfilGereja');
                  }
                }}>
                <Text
                  style={{
                    fontSize: 15,
                    fontFamily: 'Roboto-Medium',
                    color: '#b70000',
                    marginTop: 7,
                  }}>
                  {drawer.label.editGereja}
                </Text>
              </TouchableOpacity>
            </>
          )}
        </View>

        {/* Button kartu nama */}
        <TouchableOpacity
          style={{
            backgroundColor: '#f57c00',
            borderRadius: 4,
            paddingVertical: 7,
            paddingHorizontal: 18,
            marginTop: 10,
          }}
          onPress={() => props.navigation.navigate('KartuNama')}>
          <Text
            style={{
              fontSize: 15,
              fontFamily: 'Roboto-Medium',
              color: '#fff',
            }}>
            Kartu Nama
          </Text>
        </TouchableOpacity>

        {/* Separator  */}
        <View
          style={{
            width: '90%',
            height: 2,
            alignSelf: 'center',
            backgroundColor: 'rgba(0, 0, 0, 0.52)',
            marginVertical: 9,
          }}
        />

        {/* List Screens/Konten utama */}
        <DrawerItems
          {...props}
          activeTintColor="#fff"
          activeBackgroundColor="#5e0101"
          inactiveTintColor="#303030"
          inactiveBackgroundColor="#fff"
          itemsContainerStyle={{
            width: '100%',
          }}
          itemStyle={{
            borderRadius: 4,
            marginVertical: 2,
          }}
          labelStyle={{
            fontSize: 16,
            fontFamily: 'Roboto-Medium',
          }}
        />
      </ScrollView>

      {/* Separator  */}
      <View
        style={{
          width: '90%',
          height: 2,
          alignSelf: 'center',
          backgroundColor: 'rgba(0, 0, 0, 0.52)',
          marginVertical: 9,
        }}
      />

      {/* Footer */}
      <TouchableOpacity
        onPress={logout}
        style={{
          flexDirection: 'row',
          paddingVertical: 12,
          paddingLeft: 20,
          paddingRight: 12,
          backgroundColor: '#eeeeee',
          borderRadius: 4,
          alignItems: 'center',
        }}>
        <IconAwesome name="sign-out-alt" color="#5e0101" size={25} />
        <Text
          style={{
            fontSize: 16,
            fontFamily: 'Roboto-Medium',
            color: '#303030',
            marginLeft: 27,
          }}>
          Logout
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export const IconDrawer = ({name}) => {
  return <Image source={{uri: name}} style={{width: 28, height: 28}} />;
};

const styles = StyleSheet.create({
  commonRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default Drawer;
