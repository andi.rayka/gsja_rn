import React from 'react';

import {TouchableOpacity, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Fontisto';
import {HeaderButtons, HeaderButton} from 'react-navigation-header-buttons';

const NavHeaderButton = props => {
  return (
    <HeaderButton
      {...props}
      Icon
      IconComponent={Icon}
      iconSize={23}
      color={props.color}
    />
  );
};

export const NavHeaderButtons = props => {
  return (
    <HeaderButtons
      HeaderButtonComponent={NavHeaderButton}
      OverflowIcon={<Icon name="more-v-a" size={23} color="#fff" />}
      {...props}
      onOverflowMenuPress={() => alert('overflowmenu')}
    />
  );
};
export {Item} from 'react-navigation-header-buttons';

export const NotifIcon = () => {
  return (
    <TouchableOpacity>
      <Text>ada</Text>
    </TouchableOpacity>
  );
};
