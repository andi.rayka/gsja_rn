import React, {useState} from 'react';
// Component
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Modal,
  StyleSheet,
  ScrollView,
} from 'react-native';

// Micro Component
import commonStyles from 'gsja/src/assets/styles';
import warna from 'gsja/src/assets/colors';
import Icon from 'react-native-vector-icons/Fontisto';

const {flexStartRow, flexEndRow, spaceBetweenRow} = commonStyles;

// Function
import DatePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

// Header Tiruan untuk modal.
// Default height untuk android adalah 56
const HeaderTiruan = ({title, onClose}) => {
  return (
    <View style={[flexStartRow, localStyles.containerHeaderTiruan]}>
      {/* Button Back */}
      <TouchableOpacity onPress={onClose}>
        <Icon name="arrow-left" size={23} color={warna.putihAsli} />
      </TouchableOpacity>
      {/* Text judul modal */}
      <Text
        style={{
          fontFamily: 'Roboto-Regular',
          fontSize: 22,
          color: warna.putihAsli,
          marginLeft: 17,
        }}>
        {title}
      </Text>
    </View>
  );
};

// Button untuk tanggal
export const ButtonDate = ({title, saveChange, selectedDate}) => {
  const [isShowDate, toggleShowDate] = useState(false);

  const handleDate = (event, pickedDate) => {
    toggleShowDate(false);

    if (pickedDate) {
      const finalDate = moment(pickedDate).format('YYYY-MM-DD');

      saveChange(finalDate);
    }
  };

  return (
    <View>
      {/* Judul Filter  */}
      <View style={flexStartRow}>
        <Icon name="filter" size={16} color="#000" />
        <Text
          style={{
            fontFamily: 'Roboto-Medium',
            fontSize: 16,
            color: '#303030',
            marginLeft: 8,
          }}>
          {title}
        </Text>
      </View>

      {/* Button Pilihan Filter  */}
      <TouchableOpacity
        onPress={() => toggleShowDate(true)}
        style={localStyles.buttonPilihanFilter}>
        <Text>
          {selectedDate
            ? moment(selectedDate).format('DD MMMM YYYY')
            : 'Pilih Tanggal'}
        </Text>
      </TouchableOpacity>

      {isShowDate && (
        <DatePicker
          mode="date"
          value={
            selectedDate
              ? moment(selectedDate, 'YYYY-MM-DD').toDate()
              : new Date()
          }
          onChange={handleDate}
        />
      )}
    </View>
  );
};

// Button untuk memilih list
export const ButtonList = ({
  title,

  selectedList,
  listOption,
  saveChange,

  idName,
  defaultValueisIndex0,
}) => {
  const [isListOpen, toggleOpenList] = useState(false);

  // Jika default value adalah null/tidak ada, maka buat option semua
  // Yang mana adalah null/tidak ada filter
  if (!defaultValueisIndex0) {
    listOption = [{id: null, name: 'Semua'}, ...listOption];
  }

  return (
    <View>
      {/* Judul Filter  */}
      <View style={flexStartRow}>
        <Icon name="filter" size={16} color="#000" />
        <Text
          style={{
            fontFamily: 'Roboto-Medium',
            fontSize: 16,
            color: '#303030',
            marginLeft: 8,
          }}>
          {title}
        </Text>
      </View>

      {/* Button Pilihan Filter  */}
      <TouchableOpacity
        onPress={() => toggleOpenList(true)}
        style={[spaceBetweenRow, localStyles.buttonPilihanFilter]}>
        {/* Jika default value = index ke-0, maka pilih ke[0] */}
        {defaultValueisIndex0 ? (
          <Text>
            {selectedList.name ? selectedList.name : listOption[0].name}
          </Text>
        ) : (
          <Text>{selectedList.name ? selectedList.name : 'Semua'}</Text>
        )}

        <Icon name="angle-down" size={16} color={warna.hitamText2} />
      </TouchableOpacity>

      {/* Modal List */}
      <Modal
        animationType="fade"
        transparent
        visible={isListOpen}
        onRequestClose={() => toggleOpenList(false)}>
        <HeaderTiruan
          title={`Pilih ${title}`}
          onClose={() => toggleOpenList(false)}
        />

        <ScrollView
          style={{
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            backgroundColor: warna.putihAsli,
          }}>
          {listOption.map((item, key) => {
            return (
              <TouchableOpacity
                key={key}
                onPress={() => {
                  toggleOpenList(false);
                  saveChange({id: item[idName], name: item.name});
                }}
                style={localStyles.buttonListOption}>
                <Text>{item.name}</Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </Modal>
    </View>
  );
};

// Komponen Utama Modal filter
const ModalFilter = ({isModalOpen, onClose, onResetFilter, title, child}) => {
  return (
    <Modal
      animationType="fade"
      transparent
      visible={isModalOpen}
      onRequestClose={onClose}>
      {/* Isi modal  */}
      <View
        style={{
          width: Dimensions.get('window').width,
          height: Dimensions.get('window').height,
          backgroundColor: warna.putihAsli,
          paddingBottom: 23,
        }}>
        <HeaderTiruan title={title} onClose={onClose} />

        {/* Container Konten */}
        <ScrollView
          contentContainerStyle={{flexGrow: 1}}
          style={{
            backgroundColor: warna.putihAsli,
            paddingVertical: 20,
            paddingLeft: 25,
            paddingRight: 20,
          }}>
          {child}
        </ScrollView>

        {/* Button di bawah */}
        <View style={flexEndRow}>
          {/* Button hapus filter  */}
          <TouchableOpacity
            onPress={onResetFilter}
            style={localStyles.buttonHapusFilter}>
            <Text
              style={{
                fontFamily: 'Roboto-Regular',
                fontSize: 15,
                color: '#9e9e9e',
              }}>
              Hapus Filter
            </Text>
          </TouchableOpacity>

          {/* Button terapkan */}
          <TouchableOpacity
            onPress={onClose}
            style={localStyles.buttonTerapkan}>
            <Text
              style={{
                fontFamily: 'Roboto-Regular',
                fontSize: 15,
                color: warna.putihAsli,
              }}>
              Terapkan
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

// Catatan
// 1. Component ini berfungsi kalau nama key dari tiap object adalah "name"

const localStyles = StyleSheet.create({
  containerHeaderTiruan: {
    width: '100%',
    height: 56,
    backgroundColor: warna.merahHeader,
    elevation: 1,
    paddingHorizontal: 10,
  },
  buttonPilihanFilter: {
    marginTop: 7,
    marginBottom: 27,
    paddingTop: 15,
    paddingBottom: 17,
    paddingHorizontal: 27,
    borderWidth: 1,
    borderColor: '#707070',
    borderRadius: 12,
  },
  buttonHapusFilter: {
    marginTop: 5,
    marginBottom: 10,
    marginRight: 20,
    paddingTop: 9,
    paddingBottom: 11,
    paddingHorizontal: 20,
    borderRadius: 4,
    borderColor: '#bdbdbd',
    borderWidth: 3,
  },
  buttonTerapkan: {
    backgroundColor: '#2196f3',
    marginTop: 5,
    marginBottom: 10,
    marginRight: 20,
    paddingTop: 11,
    paddingBottom: 13,
    paddingHorizontal: 30,
    borderRadius: 4,
  },
  buttonListOption: {
    backgroundColor: warna.putihAsli,
    paddingTop: 15,
    paddingBottom: 17,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: warna.abuabu1,
    borderRadius: 12,
  },
});

export default ModalFilter;
