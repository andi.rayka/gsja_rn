import React, {useState} from 'react';

// Component
import {
  TouchableOpacity,
  Dimensions,
  Modal,
  TextInput,
  View,
} from 'react-native';

// Micro Component
import commonStyles from 'gsja/src/assets/styles';
import Icon from 'react-native-vector-icons/Fontisto';

const {flexStartRow, modalFreeSpace} = commonStyles;

const SearchModal = ({isModalOpen, onClose, onChangeText, placeholder}) => {
  const [text, setText] = useState('');

  return (
    <Modal
      animationType="fade"
      transparent
      visible={isModalOpen}
      onRequestClose={onClose}>
      {/* Space kosong di seluruh halaman/selain content utama */}
      {/* Jika diklik akan keluar dari modal */}
      <TouchableOpacity onPress={onClose} style={modalFreeSpace} />

      {/* Konten modal  */}
      <View
        style={{
          width: Dimensions.get('window').width,
          position: 'absolute',
          top: 0,
          backgroundColor: '#fff',
          paddingTop: 20,
          paddingBottom: 23,
          paddingHorizontal: 13,
        }}>
        {/* Container Search  */}
        <View
          style={[
            flexStartRow,
            {paddingLeft: 15, backgroundColor: '#eeeeee', borderRadius: 12},
          ]}>
          <Icon name="search" size={20} color="#9f9f9f" />
          <TextInput
            autoFocus
            style={{
              fontSize: 17,
              color: '#9f9f9f',
              fontFamily: 'Roboto-Regular',
              flex: 1,
            }}
            placeholder={`Cari ${placeholder}`}
            autoCapitalize="none"
            returnKeyType="search"
            value={text}
            onChangeText={setText}
            onSubmitEditing={() => {
              onClose();
              onChangeText(text);
            }}
          />

          {/* Button reset search */}
          <TouchableOpacity
            onPress={() => setText('')}
            style={{
              height: '100%',
              paddingLeft: 10,
              paddingRight: 14,
              justifyContent: 'center',
            }}>
            <Icon name="close-a" size={20} color="#9f9f9f" />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default SearchModal;
