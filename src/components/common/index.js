// Function
import FastImage from 'react-native-fast-image';
import {createImageProgress} from 'react-native-image-progress';

// Gambar yang bisa loading dan FastImage
export const Image = createImageProgress(FastImage);
