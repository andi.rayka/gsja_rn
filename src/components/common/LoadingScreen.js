import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import warna from 'gsja/src/assets/colors';

const LoadingScreen = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size="large" color={warna.merahHeader} animating />
    </View>
  );
};

export default LoadingScreen;
