const strings = {
  drawer: {
    label: {
      beranda: 'Beranda',
      kalender: 'Kalender Kegiatan',
      pendeta: 'Pelayan Injil',
      gereja: 'Gereja',
      institusi: 'Institusi',
      repositori: 'Repositori',
      statistik: 'Statistik',
      editProfil: 'Edit Profil',
      editGereja: 'Edit Gereja',
      bantuan: 'Bantuan Gereja',
    },
  },
};

export default strings;
