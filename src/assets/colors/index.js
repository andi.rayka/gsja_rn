const colors = {
  merahHeader: '#b70000',
  merahStatusbar: '#5e0101',
  abuabu1: '#e9e9e9',
  abuabu2: '#eaeaea',
  putihAsli: '#fff',
  hitamText: '#303030',
  hitamText2: '#505050',
  hitamAsli: '#000',
  biruLink: '#60bfff',
};

export default colors;
