import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  flexStartRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  flexEndRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  spaceBetweenRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  modalFreeSpace: {
    flex: 1,
    backgroundColor: '#000000',
    opacity: 0.6,
  },
});

export default styles;
