import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

// Untuk memanggil api
export const apiPost = async dataArrObj => {
  const loginToken = await AsyncStorage.getItem('loginToken');

  // Array berisi data untuk di-Post
  dataArrObj.forEach(({url, data}, index, arr) => {
    arr[index] = axios.post(url, data, {
      // baseURL: 'http://smart.gsja.org/index.php/api2b',
      baseURL: 'http://206.189.34.129/index.php/api2b',
      transformRequest: [
        reqData => {
          reqData.token = loginToken;

          // console.log({reqData});

          return reqData;
        },
        ...axios.defaults.transformRequest,
      ],
    });
  });

  // Response dari post. Yang akan di-return oleh function ini
  const response = await axios
    .all(dataArrObj)
    .then(resp => {
      resp.forEach((item, index, arr) => {
        const {error} = item.data;

        if (error === 0) {
          arr[index] = item.data.data.records;
        } else {
          const {error_message} = item.data;
          alert(error_message);
        }
      });

      // console.log('response', resp[0]);

      return resp;
    })
    .catch(error => {
      if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        console.warn(error.request);
      } else {
        console.warn(error.message);
      }

      return null;
    });

  return response;
};

// Untuk login dan setakses saja
export const apiAuth = async (url, data, useToken) => {
  const loginToken = await AsyncStorage.getItem('loginToken');

  // Untuk setakses yang perlu loginToken
  if (useToken) {
    data.token = loginToken;
  }

  // console.log(data);

  // Response dari post. Yang akan di-return oleh function ini
  const response = await axios
    // .post('http://smart.gsja.org/index.php/api2b' + url, data)
    .post('http://206.189.34.129/index.php/api2b' + url, data)
    .then(resp => {
      const {error} = resp.data;
      // console.log(resp.data);

      if (error === 0) {
        const {data: respData} = resp.data;

        return respData;
      } else {
        const {error_message} = resp.data;
        alert(error_message);
      }
    })
    .catch(error => {
      if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        console.warn(error.request);
      } else {
        console.warn(error.message);
      }

      return null;
    });

  return response;
};
