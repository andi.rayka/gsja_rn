import React from 'react';

// Component
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Image} from 'gsja/src/components/common';

// Micro Component
import Icon from 'react-native-vector-icons/Fontisto';
import rowStyles from 'gsja/src/assets/styles';

const {flexStartRow} = rowStyles;

// Function
import {withNavigation} from 'react-navigation';

const ListPendetaItem = ({item, navigation: {navigate}, isAdmin}) => {
  if (isAdmin) {
    return (
      // Container Utama
      <TouchableOpacity
        activeOpacity={0.6}
        style={[
          flexStartRow,
          {
            backgroundColor: '#fff',
            paddingHorizontal: 10,
            paddingVertical: 9,
            elevation: 3,
          },
        ]}
        onPress={() => navigate('DetailPendeta', {id_pendeta: item.idmember})}>
        {/* Foto Profil Pendeta  */}
        <Image
          source={{uri: item.image}}
          style={{width: 100, height: 100, marginBottom: 18}}
        />

        {/* Bagian Biodata Pendeta  */}
        <View style={{marginLeft: 12, flex: 1}}>
          {/* Nama pendeta  */}
          <Text
            style={{
              color: '#303030',
              fontSize: 16,
              fontFamily: 'Roboto-Medium',
            }}>
            {item.firstname}
          </Text>

          {/* Nama gereja  */}
          <Text style={[styles.textBiodata, {lineHeight: 18}]}>
            {item.name}
          </Text>

          {/* Jabatan Pendeta  */}
          <Text style={[styles.textBiodata, {lineHeight: 18}]}>
            {item.titlename}
          </Text>

          {/* Wilayah BPD Pendeta  */}
          <Text style={[styles.textBiodata, {marginBottom: 10}]}>
            BPD: {item.wilayah}
          </Text>
        </View>
      </TouchableOpacity>
    );
  } else {
    return (
      // Container Utama
      <View
        activeOpacity={0.6}
        style={[
          flexStartRow,
          {
            backgroundColor: '#fff',
            paddingHorizontal: 10,
            paddingVertical: 9,
            elevation: 3,
          },
        ]}>
        {/* Foto Profil Pendeta  */}
        <Image
          source={{uri: item.image}}
          style={{width: 100, height: 100, marginBottom: 18}}
        />

        {/* Bagian Biodata Pendeta  */}
        <View style={{marginLeft: 12, flex: 1}}>
          {/* Nama pendeta  */}
          <Text
            style={{
              color: '#303030',
              fontSize: 16,
              fontFamily: 'Roboto-Medium',
            }}>
            {item.firstname}
          </Text>

          {/* Nama gereja  */}
          <Text style={[styles.textBiodata, {lineHeight: 18}]}>
            {item.name}
          </Text>

          {/* Jabatan Pendeta  */}
          <Text style={[styles.textBiodata, {lineHeight: 18}]}>
            {item.titlename}
          </Text>

          {/* Wilayah BPD Pendeta  */}
          <Text style={[styles.textBiodata, {marginBottom: 10}]}>
            BPD: {item.wilayah}
          </Text>
        </View>
      </View>
    );
  }
};

// Separator/Pembatas antar item
export const itemSeparator = () => (
  <View style={{backgroundColor: '#e9e9e9', height: 11, width: '100%'}} />
);

const styles = StyleSheet.create({
  textBiodata: {
    color: '#303030',
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
  },
});

export default withNavigation(ListPendetaItem);
