import React, {useState, useEffect, useRef} from 'react';

// Component
import MainContainer from 'gsja/src/components/container/FlatlistContainer';
import ModalFilter, {
  ButtonList,
} from 'gsja/src/components/filterData/ModalFilter';
import ModalSearch from 'gsja/src/components/filterData/ModalSearch';
import ListItem, {itemSeparator} from './Item';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import strings from 'gsja/src/assets/strings';

const {drawer} = strings;

// Function
import {apiPost} from 'gsja/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';

const ListPendeta = ({navigation: {setParams, state}}) => {
  // Input Filter
  const [postFilter, setPostFilter] = useState({
    keyword: '',
    wilayah: {id: null, name: null},
  });

  // List Pilihan Filter
  const [listWilayah, setListWilayah] = useState([]);

  // Modal
  const [isModalSearchOpen, toggleOpenModalSearch] = useState(false);
  const [isModalFilterOpen, toggleOpenModalFilter] = useState(false);

  // cek user role admin
  const [isAdmin, setIsAdmin] = useState(false);

  // Menghubungkan button di header dengan function di dlm komponen
  const {params = {}} = state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);
  // Bind button with function
  useEffect(() => {
    setParam.current({openModalSearch: () => toggleOpenModalSearch(true)});
    setParam.current({openModalFilter: () => toggleOpenModalFilter(true)});
  }, [paramDesc, setParam]);

  // initial fetch list filter
  useEffect(() => {
    let postData = [
      {
        url: '/getListWilayah',
        data: {},
      },
    ];

    const cekIsAdmin = async () => {
      const dataIsAdmin = await AsyncStorage.getItem('isAdmin');
      if (dataIsAdmin === '1') {
        setIsAdmin(true);
      }
    };

    cekIsAdmin();

    apiPost(postData).then(resp => setListWilayah(resp[0]));
  }, []);

  return (
    <MainContainer
      url="/getListPendeta"
      additionalPostData={{
        ...postFilter,
        wilayah: postFilter.wilayah.id,
      }}
      renderItem={({item}) => <ListItem item={item} isAdmin={isAdmin} />}
      separatorItem={itemSeparator}
      loadingScreenException={
        <>
          <ModalSearch
            isModalOpen={isModalSearchOpen}
            onClose={() => toggleOpenModalSearch(false)}
            placeholder="nama Pelayan Injil"
            onChangeText={text => setPostFilter({...postFilter, keyword: text})}
          />

          <ModalFilter
            isModalOpen={isModalFilterOpen}
            onClose={() => toggleOpenModalFilter(false)}
            onResetFilter={() =>
              setPostFilter({
                ...postFilter,
                wilayah: {id: null, name: null},
              })
            }
            title="Filter List Pendeta"
            child={
              <ButtonList
                defaultValueisIndex0
                title="Wilayah"
                selectedList={postFilter.wilayah}
                listOption={listWilayah}
                saveChange={obj => setPostFilter({...postFilter, wilayah: obj})}
                idName="idorganization"
              />
            }
          />
        </>
      }
    />
  );
};

ListPendeta.navigationOptions = ({navigation: {toggleDrawer, getParam}}) => ({
  headerTitle: drawer.label.pendeta,
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="drawer"
        color="#fff"
        iconName="nav-icon"
        onPress={toggleDrawer}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="filter"
        color="#fff"
        iconName="filter"
        onPress={getParam('openModalFilter')}
      />
      <Item
        title="search"
        color="#fff"
        iconName="search"
        onPress={getParam('openModalSearch')}
      />
    </NavHeaderButtons>
  ),
});

export default ListPendeta;
