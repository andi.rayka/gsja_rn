import React, {useState, useEffect, useRef} from 'react';

// Component
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Modal,
  Linking,
  Dimensions,
} from 'react-native';
import DetailComponent, {
  TabelDataTwoRows,
} from 'gsja/src/components/container/DetailContainer';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import IconAwesome from 'react-native-vector-icons/FontAwesome5';
import warna from 'gsja/src/assets/colors';

// Function
import {apiPost} from 'gsja/src/services/api';
import moment from 'moment';

const DetailGereja = ({navigation: {getParam, setParams, state}}) => {
  const [isLoading, setLoading] = useState(true);
  const [fetchedData, setFetchedData] = useState([]);
  const [isModalOpen, toggleOpenModal] = useState(false);

  // Menghubungkan button di header dengan function di dlm komponen
  const {params = {}} = state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);
  // Bind button with function
  useEffect(() => {
    setParam.current({openModal: () => toggleOpenModal(true)});
  }, [paramDesc, setParam]);

  // initial fetch detail gereja
  useEffect(() => {
    const id_gereja = getParam('id_gereja');
    let postData = [
      {
        url: '/getDetailGereja',
        data: {
          id_gereja,
        },
      },
    ];

    apiPost(postData)
      .then(resp => setFetchedData(resp[0]))
      .then(() => setLoading(false));
  }, [getParam]);

  // Modal Untuk Akses Goggle Maps
  const ModalLocation = () => {
    return (
      <Modal
        animationType="fade"
        transparent
        visible={isModalOpen}
        onRequestClose={() => toggleOpenModal(false)}>
        {/* Space kosong di seluruh halaman/selain content modal  */}
        <TouchableOpacity
          onPress={() => toggleOpenModal(false)}
          style={styles.containerFreespace}
        />

        {/* Konten modal  */}
        <View
          style={{
            flexDirection: 'row',
            width: Dimensions.get('window').width,
            backgroundColor: warna.putihAsli,
          }}>
          {/* Tunjukkan arah/direction gereja */}
          <TouchableOpacity
            onPress={() =>
              getLocationDirection(fetchedData.location, fetchedData.name)
            }
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 20,
              paddingBottom: 16,
            }}>
            <IconAwesome
              name="directions"
              size={37}
              color={warna.merahHeader}
            />
            <Text
              style={{
                color: warna.hitamText,
                fontSize: 14,
                fontFamily: 'Roboto-Regular',
              }}>
              Tunjukkan Arah
            </Text>
          </TouchableOpacity>

          {/* Search gereja di google map */}
          <TouchableOpacity
            onPress={() =>
              searchLocationByGMap(fetchedData.location, fetchedData.name)
            }
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 20,
              paddingBottom: 16,
            }}>
            <IconAwesome
              name="map-marker-alt"
              size={37}
              color={warna.merahHeader}
            />
            <Text
              style={{
                color: warna.hitamText,
                fontSize: 14,
                fontFamily: 'Roboto-Regular',
              }}>
              Tampilkan Peta
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  const {tglulangtahun, rt, rw, u17, a17} = fetchedData;

  // Daftar Detail Data
  const detailData = [
    ['Kode Gereja', fetchedData.code],
    ['Tanggal Berdiri', moment(tglulangtahun).format('DD MMMM YYYY')],
    ['Gembala Sidang', fetchedData.gembala_sidang_text],
    ['BPD Gereja', fetchedData.namabpd],
    ['Provinsi', fetchedData.namaprovinsi],
    ['Alamat', fetchedData.address],
    ['RT / RW', rt && rw && `${rt} / ${rw}`],
    ['Email', fetchedData.email],
    ['Telp.', fetchedData.phone],
    ['Fax.', fetchedData.fax],
  ];

  return (
    <DetailComponent
      isLoading={isLoading}
      imageURL={fetchedData.image}
      detailName={fetchedData.name}
      detailData={detailData}
      komponenDiBawah={
        <>
          <Text
            style={{
              color: warna.hitamText,
              fontSize: 17,
              fontFamily: 'Roboto-Medium',
            }}>
            Jadwal Gereja
          </Text>
          <Text
            style={{
              color: warna.hitamText,
              fontSize: 16,
              fontFamily: 'Roboto-Regular',
            }}>
            {fetchedData.jadwal ? fetchedData.jadwal : '-'}
          </Text>
          <ModalLocation />
        </>
      }
      containerTambahan={
        <>
          <TabelDataTwoRows
            judul="Jumlah Jemaat"
            leftHead=""
            rightHead="Jumlah"
            contentData={[
              {kiri: 'Usia >= 17 tahun', kanan: a17 ? a17 : '-'},
              {kiri: 'Usia < 17 tahun', kanan: u17 ? u17 : '-'},
              {
                kiri: 'Total',
                kanan: a17 && u17 ? parseInt(u17, 10) + parseInt(a17, 10) : '-',
              },
            ]}
            leftKeyname="kiri"
            rightKeyname="kanan"
          />

          <Text
            style={{
              marginTop: 5,
              color: '#505050',
              fontSize: 16,
              fontFamily: 'Roboto-Medium',
            }}>
            Jumlah KKA : {fetchedData.jumlahkka}
          </Text>

          <TabelDataTwoRows
            judul="Pelayan Injil"
            leftHead="Nama"
            rightHead="Jabatan"
            contentData={fetchedData.listpi}
            leftKeyname="firstname"
            rightKeyname="titlename"
          />
        </>
      }
    />
  );
};

// Handle search lokasi dengan aplikasi google map
const searchLocationByGMap = (lat_long, plainSearchParam) => {
  // const searchParam = plainSearchParam.split(' ').join('+');
  const searchParam = lat_long
    ? lat_long.replace('_', ',')
    : plainSearchParam.split(' ').join('+');
  let searchUrl = `https://www.google.com/maps/search/?api=1&query=${searchParam}`;

  Linking.canOpenURL(searchUrl)
    .then(supported => {
      if (!supported) {
        alert('Device tidak mendukung untuk membuka maps');
        console.log('Gagal handle url: ' + searchUrl);
      } else {
        return Linking.openURL(searchUrl);
      }
    })
    .catch(err => console.error('An error occurred', err));
};

// Handle get direction lokasi dengan aplikasi google map
// Param -> latitude dan longitude, tempat yang dicari
const getLocationDirection = (lat_long, plainUrlParam) => {
  // Jika latitude dan longitude adalah null, maka cari berdasarkan nama
  const directionParam = lat_long
    ? lat_long.replace('_', ',')
    : plainUrlParam.split(' ').join('+');

  const directionUrl = `https://www.google.com/maps/dir/?api=1&destination=${directionParam}`;

  Linking.canOpenURL(directionUrl)
    .then(supported => {
      if (!supported) {
        console.log("Can't handle url: " + directionUrl);
      }
      return Linking.openURL(directionUrl);
    })
    .catch(err => console.error('An error occurred', err));
};

DetailGereja.navigationOptions = ({navigation: {goBack, getParam}}) => ({
  headerTitle: 'Detail Gereja',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color={warna.putihAsli}
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="map"
        color={warna.putihAsli}
        iconName="map-marker-alt"
        onPress={getParam('openModal')}
      />
    </NavHeaderButtons>
  ),
});

const styles = StyleSheet.create({
  containerFreespace: {
    flex: 1,
    backgroundColor: warna.hitamAsli,
    opacity: 0.6,
  },
});

export default DetailGereja;
