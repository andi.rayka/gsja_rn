import React, {useState, useEffect, useRef} from 'react';

// Component
import MainContainer from 'gsja/src/components/container/FlatlistContainer';
import ModalFilter, {
  ButtonList,
} from 'gsja/src/components/filterData/ModalFilter';
import ModalSearch from 'gsja/src/components/filterData/ModalSearch';
import ListItem, {itemSeparator} from './Item';
import {Linking} from 'react-native';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import strings from 'gsja/src/assets/strings';

const {drawer} = strings;

// Function
import {apiPost} from 'gsja/src/services/api';

const ListGereja = ({navigation: {setParams, state}}) => {
  // Input Filter
  const [postFilter, setPostFilter] = useState({
    keyword: '',
    wilayah: {id: null, name: null},
    status_sidang: {id: null, name: null},
  });

  // List Pilihan Filter
  const [listWilayah, setListWilayah] = useState([]);
  const [listStatusGereja, setListStatusGereja] = useState([]);

  // Modal
  const [isModalSearchOpen, toggleOpenModalSearch] = useState(false);
  const [isModalFilterOpen, toggleOpenModalFilter] = useState(false);

  // Menghubungkan button di header dengan function di dlm komponen
  const {params = {}} = state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);
  // Bind button with function
  useEffect(() => {
    setParam.current({openModalSearch: () => toggleOpenModalSearch(true)});
    setParam.current({openModalFilter: () => toggleOpenModalFilter(true)});
    setParam.current({searchLocationByGMap});
  }, [paramDesc, setParam]);

  // initial fetch list filter
  useEffect(() => {
    let postData = [
      {
        url: '/getListWilayah',
        data: {},
      },
      {
        url: '/getListStatusGereja',
        data: {},
      },
    ];

    apiPost(postData).then(resp => {
      setListWilayah(resp[0]);
      setListStatusGereja(resp[1]);
    });
  }, []);

  // Handle search lokasi dengan aplikasi google map
  const searchLocationByGMap = () => {
    let searchUrl = 'https://www.google.com/maps/search/?api=1&query=GSJA';

    Linking.canOpenURL(searchUrl)
      .then(supported => {
        if (!supported) {
          console.log('Gagal handle url: ' + searchUrl);
        } else {
          return Linking.openURL(searchUrl);
        }
      })
      .catch(err => console.error('An error occurred', err));
  };

  return (
    <MainContainer
      url="/getListGereja"
      additionalPostData={{
        ...postFilter,
        wilayah: postFilter.wilayah.id,
        status_sidang: postFilter.status_sidang.id,
      }}
      renderItem={({item}) => <ListItem item={item} />}
      separatorItem={itemSeparator}
      loadingScreenException={
        <>
          <ModalSearch
            isModalOpen={isModalSearchOpen}
            onClose={() => toggleOpenModalSearch(false)}
            placeholder="nama gereja"
            onChangeText={text => setPostFilter({...postFilter, keyword: text})}
          />

          <ModalFilter
            isModalOpen={isModalFilterOpen}
            onClose={() => toggleOpenModalFilter(false)}
            onResetFilter={() =>
              setPostFilter({
                ...postFilter,
                wilayah: {id: null, name: null},
                status_sidang: {id: null, name: null},
              })
            }
            title="Filter List Gereja"
            child={
              <>
                <ButtonList
                  defaultValueisIndex0
                  title="Wilayah"
                  selectedList={postFilter.wilayah}
                  listOption={listWilayah}
                  saveChange={obj =>
                    setPostFilter({...postFilter, wilayah: obj})
                  }
                  idName="idorganization"
                />

                <ButtonList
                  title="Status Gereja"
                  selectedList={postFilter.status_sidang}
                  listOption={listStatusGereja}
                  saveChange={obj =>
                    setPostFilter({...postFilter, status_sidang: obj})
                  }
                  idName="idlevel"
                />
              </>
            }
          />
        </>
      }
    />
  );
};

ListGereja.navigationOptions = ({navigation: {toggleDrawer, getParam}}) => ({
  headerTitle: drawer.label.gereja,
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="drawer"
        color="#fff"
        iconName="nav-icon"
        onPress={toggleDrawer}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="nearby"
        color="#fff"
        iconName="map-marker-alt"
        onPress={getParam('searchLocationByGMap')}
      />
      <Item
        title="filter"
        color="#fff"
        iconName="filter"
        onPress={getParam('openModalFilter')}
      />
      <Item
        title="search"
        color="#fff"
        iconName="search"
        onPress={getParam('openModalSearch')}
      />
    </NavHeaderButtons>
  ),
});

export default ListGereja;
