import React, {useState, useEffect} from 'react';

// Component
import {View, Text, StatusBar, ScrollView} from 'react-native';
import LoadingScreen from 'gsja/src/components/common/LoadingScreen';
import {Image} from 'gsja/src/components/common';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import warna from 'gsja/src/assets/colors';

// Function
import {apiPost} from 'gsja/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

const KartuNama = () => {
  const [isLoading, setLoading] = useState(false);
  const [fetchedData, setFetchedData] = useState([]);
  const [fetchedBranchdetail, setBranchdetail] = useState({});

  // initial fetch detail gereja
  useEffect(() => {
    const initialFetch = async () => {
      setLoading(true);
      const id_pendeta = await AsyncStorage.getItem('idUser');
      let postData = [
        {
          url: '/getKartuNama',
          data: {
            id_pendeta,
          },
        },
      ];

      apiPost(postData)
        .then(resp => {
          // Karena gambar kalau linknya sama, nggak akan ganti.
          // Makanya diakali pakai param waktu untuk lihat gambar
          // Biar tiap buka refresh
          const waktu = moment().unix();
          resp[0].image += `?waktuInsert=${waktu}`;

          setFetchedData(resp[0]);
          setBranchdetail(resp[0].branchdetail);
        })
        .then(() => setLoading(false));
    };

    initialFetch();
  }, []);

  // Return QR Code URL
  const QRLink = link =>
    `https://api.qrserver.com/v1/create-qr-code/?data=${link}`;

  return (
    <ScrollView
      contentContainerStyle={{
        flexGrow: 1,
        backgroundColor: '#e9e9e9',
        padding: 5,
      }}>
      <StatusBar backgroundColor={warna.merahStatusbar} />

      {isLoading ? (
        <LoadingScreen />
      ) : (
        <View style={{borderRadius: 20, overflow: 'hidden'}}>
          <View
            style={{
              backgroundColor: warna.merahStatusbar,
              alignItems: 'center',
              paddingTop: 10,
            }}>
            <Image
              source={{uri: 'img_gsja_header'}}
              style={{width: 300, height: 50}}
            />

            {/* Foto Profil Pendeta */}
            <Image
              source={{uri: fetchedData.image}}
              style={{width: 180, height: 180, marginTop: 10}}
            />

            {/* Container Biodata Pendeta */}
            <View
              style={{
                marginTop: 10,
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: 5,
                paddingVertical: 7,
                width: '100%',
              }}>
              {/* Nama Pendeta */}
              <Text
                style={{
                  color: warna.putihAsli,
                  fontSize: 22,
                  fontFamily: 'Roboto-Medium',
                  lineHeight: 32,
                  textAlign: 'center',
                }}>
                {fetchedData.firstname}
              </Text>

              {/* Username Pendeta */}
              <Text
                style={{
                  color: warna.putihAsli,
                  fontSize: 22,
                  fontFamily: 'Roboto-Regular',
                }}>
                {fetchedBranchdetail.username}
              </Text>
            </View>
          </View>

          {/* QR CODE */}
          <View
            style={{
              backgroundColor: warna.merahHeader,
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 15,
              paddingBottom: 20,
            }}>
            <View style={{backgroundColor: 'white', padding: 7}}>
              <Image
                source={{uri: QRLink(fetchedData.username)}}
                style={{width: 200, height: 200}}
              />
            </View>
          </View>
        </View>
      )}
    </ScrollView>
  );
};

KartuNama.navigationOptions = ({navigation: {toggleDrawer}}) => ({
  headerTitle: 'Kartu Nama',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="drawer"
        color={warna.putihAsli}
        iconName="nav-icon"
        onPress={toggleDrawer}
      />
    </NavHeaderButtons>
  ),
});

export default KartuNama;
