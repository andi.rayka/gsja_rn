import React, {useState, useEffect} from 'react';

// Component
import DetailComponent, {
  TabelDataTwoRows,
  TableDataTambahan,
} from 'gsja/src/components/container/DetailContainer';
import ButtonEdit from 'gsja/src/components/button/ButtonEditProfil';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import warna from 'gsja/src/assets/colors';

// Function
import {apiPost} from 'gsja/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

const DetailProfil = ({navigation: {navigate}}) => {
  const [isLoading, setLoading] = useState(true);
  const [fetchedData, setFetchedData] = useState([]);

  // initial fetch detail Pendeta
  useEffect(() => {
    const initialFetch = async () => {
      const id_pendeta = await AsyncStorage.getItem('idUser');
      let postData = [
        {
          url: '/getProfile',
          data: {
            id_pendeta,
          },
        },
      ];

      apiPost(postData)
        .then(resp => {
          // Karena gambar kalau linknya sama, nggak akan ganti.
          // Makanya diakali pakai param waktu untuk lihat gambar
          // Biar tiap buka refresh
          const waktu = moment().unix();
          resp[0].image += `?waktuInsert=${waktu}`;

          // Data yang diberikan untuk table data tambahan
          // Ambil key yang diperlukan untuk array datanya
          resp[0].rows_riwayat_jenjang.forEach((item, index, arr) => {
            const {fromyear, usia, info, namajenjang, namaorganisasi} = item;

            arr[index] = [fromyear, usia, info, namajenjang, namaorganisasi];
          });

          setFetchedData(resp[0]);
        })
        .then(() => setLoading(false));
    };

    initialFetch();
  }, []);

  const {rt, rw, iskota} = fetchedData;

  // Status pernikahan sesuai inisial
  const statusPernikahan = () => {
    switch (fetchedData.status_pernikahan) {
      case 'B':
        return 'Belum Kawin';
      case 'M':
        return 'Kawin';
      case 'D':
        return 'Duda';
      case 'J':
        return 'Janda';

      default:
        return '';
    }
  };

  // Daftar Detail Data
  const detailData = [
    ['Alias', fetchedData.alias],
    ['Kode Pelayan', fetchedData.code_pi],
    ['Tanggal Lahir', moment(fetchedData.birthdate).format('DD MMMM YYYY')],
    ['Tempat Lahir', fetchedData.birthplace],
    ['Jenis Kelamin', fetchedData.gender === 'L' ? 'Laki-Laki' : 'Perempuan'],
    ['Golongan Darah', fetchedData.golongan_darah],
    ['Status Perkawinan', statusPernikahan()],
    ['Alamat', fetchedData.address],
    ['Kelurahan', fetchedData.kelurahan],
    ['Kecamatan', fetchedData.kecamatan],
    ['Kota', fetchedData.kota],
    ['Provinsi', fetchedData.nameprovince],
    ['Kode Pos', fetchedData.postcode],
    ['RT / RW', rt && rw && `${rt} / ${rw}`],
    [
      'Pendidikan Umum Terakhir',
      fetchedData.pendidikanumum == 'SA'
        ? 'Sekolah Alkitab (Setara SMA / SMU)'
        : fetchedData.pendidikanumum == 'STTS'
        ? 'S1'
        : fetchedData.pendidikanumum,
    ],
    ['Pendidikan Teologia Terakhir', fetchedData.pendidikanteologi],
    ['Gelar Pendidikan', fetchedData.gelarpendidikan],
    ['Email', fetchedData.email],
    ['Telp.', fetchedData.phone],
    ['HP.', fetchedData.hp],
  ];

  // Jika bukan kota, maka ganti kelurahan dan kota dengan desa dan kabupaten
  if (iskota == 0) {
    detailData.splice(7, 1, ['Desa', fetchedData.desa]);
    detailData.splice(9, 1, ['Kabupaten', fetchedData.kabupaten]);
  }

  return (
    <DetailComponent
      isLoading={isLoading}
      imageURL={fetchedData.image}
      detailName={fetchedData.firstname}
      detailData={detailData}
      komponenDiBawah={
        <ButtonEdit
          onPress={() => navigate('EditProfil', {profileData: fetchedData})}
          text="Edit Profil"
        />
      }
      containerTambahan={
        <>
          <TabelDataTwoRows
            judul="Riwayat Pelayanan Gereja"
            leftHead="Tahun"
            rightHead="Gereja"
            contentData={fetchedData.rows_riwayat_gereja}
            leftKeyname="newyear"
            rightKeyname="info"
          />

          <TabelDataTwoRows
            judul="Riwayat Pelayanan Organisasi"
            leftHead="Tahun"
            rightHead="Organisasi"
            contentData={fetchedData.rows_riwayat_organisasi}
            leftKeyname="newyear"
            rightKeyname="namaorganisasi"
          />

          <TabelDataTwoRows
            judul="Riwayat Jenjang Pendidikan"
            leftHead="Tahun"
            rightHead="Pendidikan/Pelatihan/Seminar"
            contentData={fetchedData.rows_riwayat_pendidikan}
            leftKeyname="fromyear"
            rightKeyname="info"
          />

          {/* Table tambahan jumlah kolomnya lebih banyak */}
          <TableDataTambahan
            judul="Riwayat Jenjang Kependetaan"
            headerData={['Tahun', 'Usia', 'No Beslit', 'Jenjang', 'Wilayah']}
            contentData={fetchedData.rows_riwayat_jenjang}
            widthArrPerColumn={[100, 100, 100, 100, 100]}
          />
        </>
      }
    />
  );
};

DetailProfil.navigationOptions = ({navigation: {toggleDrawer}}) => ({
  headerTitle: 'Detail Profil',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="drawer"
        color={warna.putihAsli}
        iconName="nav-icon"
        onPress={toggleDrawer}
      />
    </NavHeaderButtons>
  ),
});

export default DetailProfil;
