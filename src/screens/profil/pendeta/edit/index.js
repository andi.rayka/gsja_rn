import React, {useState, useEffect} from 'react';

// Component
import {View, TouchableOpacity, Text} from 'react-native';
import MainContainer from 'gsja/src/components/container/ScrollviewContainer';
import {
  InputText,
  InputNumber,
  DropdownInput,
  DateInput,
  ChooseIsKota,
  BigList,
} from 'gsja/src/components/form/EditProfil';
import ButtonEdit from 'gsja/src/components/button/ButtonEditProfil';
import {Image} from 'gsja/src/components/common';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import Icon from 'react-native-vector-icons/FontAwesome5';
import warna from 'gsja/src/assets/colors';
import strings from 'gsja/src/assets/strings';
import rowStyles from 'gsja/src/assets/styles';

const {drawer} = strings;
const {spaceBetweenRow} = rowStyles;

// Function
import {apiPost} from 'gsja/src/services/api';
// import ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import {StackActions, NavigationActions} from 'react-navigation';
import {setAsyncStorage} from 'gsja/src/commonFunctions';

const EditProfil = ({navigation: {getParam, dispatch}}) => {
  const [editData, setEditData] = useState(getParam('profileData'));
  const [isLoading, setLoading] = useState(true);

  // Input data yang pakai list
  const [wilayah, setWilayah] = useState({
    idprovince: null,
    nameprovince: null,
  });

  // List Pilihan Filter
  const [listWilayah, setListWilayah] = useState([]);

  // initial fetch
  useEffect(() => {
    setLoading(true);
    let postData = [
      {
        url: '/getListPropinsi',
        data: {},
      },
    ];

    apiPost(postData).then(resp => {
      setListWilayah(resp[0]);

      // Set provinsi
      const {idprovince, nameprovince} = editData;
      setWilayah({
        idprovince: idprovince ? idprovince : null,
        nameprovince: nameprovince ? nameprovince : 'Pilih Provinsi',
      });
    });
    setLoading(false);
  }, [editData]);

  // Ketika buton update profile diklik
  const updateProfile = () => {
    setLoading(true);

    let postData = [
      {
        url: '/updateProfile',
        data: {
          id_pendeta: editData.idmember,
          nama: editData.firstname,
          gender: editData.gender,
          golongandarah: editData.golongan_darah,
          tempatlahir: editData.birthplace,
          tanggallahir: editData.birthdate,
          pendidikanumum: editData.pendidikanumum,
          pendidikanteologi: editData.pendidikanteologi,
          gelar: editData.gelarpendidikan,
          alamat: editData.address,
          RT: editData.rt,
          RW: editData.rw,
          iskota: editData.iskota,
          desa: editData.desa,
          kota: editData.kota,
          kelurahan: editData.kelurahan,
          kecamatan: editData.kecamatan,
          kabupaten: editData.kabupaten,
          kodepos: editData.postcode,
          propinsi: wilayah.idprovince,
          telepon: editData.phone,
          handphone: editData.hp,
          email: editData.email,
          perkawinan: editData.status_pernikahan,
          image: editData.base64Image,
          alias: editData.alias,
        },
      },
    ];

    apiPost(postData).then(([resp]) => {
      if (resp.data) {
        alert(resp.data.error_message);
      } else {
        setAsyncStorage('userData', JSON.stringify(resp));

        // Jika edit profil berhasil, kembali ke route stack awal, yaitu detail profil
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'DetailProfil'})],
        });

        dispatch(resetAction);
        alert('Berhasil update profil');
      }
    });
    setLoading(false);
  };

  // Ganti Foto Profil
  const getImageFromGallery = () => {
    // ImagePicker.launchImageLibrary({mediaType: 'photo'}, resp => {
    //   const {uri, data, didCancel, error} = resp;

    //   if (didCancel || error) {
    //     return null;
    //   }

    //   setEditData({...editData, image: uri, base64Image: data});
    // });
    ImagePicker.openPicker({
      width: 768,
      height: 768,
      cropping: true,
      cropperCircleOverlay: true,
      freeStyleCropEnabled: true,
      avoidEmptySpaceAroundImage: true,
      includeBase64: true,
    })
      .then(resp => {
        const {path, data} = resp;
        setEditData({...editData, image: path, base64Image: data});
      })
      .catch(e => console.log('error img :', e));
  };

  return (
    <MainContainer
      isLoading={isLoading}
      children={
        <>
          {/* Foto Profil */}
          <View style={{width: 160, height: 160, alignSelf: 'center'}}>
            <View style={{borderRadius: 160, overflow: 'hidden'}}>
              <Image
                source={{uri: editData.image}}
                style={{width: 160, height: 160, borderRadius: 160}}
              />
            </View>
            {/* Tombol Edit Foto  */}
            <TouchableOpacity
              onPress={getImageFromGallery}
              style={{
                backgroundColor: warna.merahHeader,
                padding: 9,
                borderRadius: 50,
                position: 'absolute',
                bottom: 0,
                right: 0,
              }}>
              <Icon name="pen" size={22} color="#fff" />
            </TouchableOpacity>
          </View>

          {/* Form Starts here */}
          {/* Nama  */}
          <InputText
            label="Nama"
            value={editData.firstname}
            onChange={newText => setEditData({...editData, firstname: newText})}
          />

          {/* Alias  */}
          <InputText
            label="Alias"
            value={editData.alias}
            onChange={newText => setEditData({...editData, alias: newText})}
          />

          {/* Tanggal Lahir */}
          <DateInput
            label="Tanggal Lahir"
            selectedDate={editData.birthdate}
            onSelect={newDate => setEditData({...editData, birthdate: newDate})}
          />

          {/* Tempat Lahir  */}
          <InputText
            label="Tempat Lahir"
            value={editData.birthplace}
            onChange={newText =>
              setEditData({...editData, birthplace: newText})
            }
          />

          {/* Status Sidang */}
          <DropdownInput
            label="Jenis Kelamin"
            selectedValue={editData.gender}
            onValueChange={(value, index) =>
              setEditData({...editData, gender: value})
            }
            arrayItem={[['Laki-Laki', 'L'], ['Perempuan', 'P']]}
          />

          {/* Golongan Darah */}
          <DropdownInput
            label="Golongan Darah"
            selectedValue={editData.golongan_darah}
            onValueChange={(value, index) =>
              setEditData({...editData, golongan_darah: value})
            }
            arrayItem={[['A', 'A'], ['B', 'B'], ['AB', 'AB'], ['O', 'O']]}
          />

          {/* Status Perkawinan */}
          <DropdownInput
            label="Status Perkawinan"
            selectedValue={editData.status_pernikahan}
            onValueChange={(value, index) =>
              setEditData({...editData, status_pernikahan: value})
            }
            arrayItem={[
              ['Belum Kawin', 'B'],
              ['Kawin', 'M'],
              ['Duda', 'D'],
              ['Janda', 'J'],
            ]}
          />

          {/* Alamat */}
          <InputText
            label="Alamat"
            value={editData.address}
            onChange={newText => setEditData({...editData, address: newText})}
          />

          {/* Pilih apakah kota atau non-kota */}
          <ChooseIsKota stateName={editData} setStateName={setEditData} />

          {/* Pilih Provinsi */}
          <BigList
            label="Provinsi"
            listOption={listWilayah}
            selectedList={wilayah}
            saveChange={setWilayah}
          />

          {/* Kode Pos */}
          <InputText
            label="Kode Pos"
            value={editData.postcode}
            onChange={newText => setEditData({...editData, postcode: newText})}
          />

          {/* RT/RW */}
          <View style={[{flex: 1}, spaceBetweenRow]}>
            {/* RT */}
            <InputText
              label="RT"
              value={editData.rt}
              onChange={newText => setEditData({...editData, rt: newText})}
            />

            {/* Garis miring pemisah */}
            <Text
              style={{
                paddingTop: 3,
                fontSize: 17,
                color: warna.hitamText,
                fontFamily: 'Roboto-Medium',
                marginHorizontal: 15,
              }}>
              /
            </Text>

            {/* RW */}
            <InputText
              label="RW"
              value={editData.rw}
              onChange={newText => setEditData({...editData, rw: newText})}
            />
          </View>

          {/* Pendidikan Umum */}
          <DropdownInput
            label="Pendidikan Umum"
            selectedValue={editData.pendidikanumum}
            onValueChange={(value, index) =>
              setEditData({...editData, pendidikanumum: value})
            }
            arrayItem={[
              ['SD', 'SD'],
              ['SMP', 'SMP'],
              ['SMA', 'SMA'],
              ['D1-D3', 'D'],
              ['S1', 'S1'],
              ['S2', 'S2'],
              ['S3', 'S3'],
            ]}
          />

          {/* Pendidikan Teologi */}
          <DropdownInput
            label="Pendidikan Teologia"
            selectedValue={editData.pendidikanteologi}
            onValueChange={(value, index) =>
              setEditData({...editData, pendidikanteologi: value})
            }
            arrayItem={[
              ['Sekolah Alkitab (Setara SMA / SMU)', 'SA'],
              ['Sekolah Tinggi Teologia / D1-D3', 'STTD'],
              ['Sekolah Tinggi Teologia / S1', 'STTS'],
              ['S2', 'S2'],
              ['S3', 'S3'],
            ]}
          />

          {/* Gelar Pendidikan */}
          <InputText
            label="Gelar Pendidikan"
            value={editData.gelarpendidikan}
            onChange={newText =>
              setEditData({...editData, gelarpendidikan: newText})
            }
          />

          {/* Email */}
          <InputText
            label="Email"
            value={editData.email}
            onChange={newText => setEditData({...editData, email: newText})}
          />

          {/* Phone */}
          <InputNumber
            label="Telp."
            value={editData.phone}
            onChange={newText => setEditData({...editData, phone: newText})}
          />

          {/* hp */}
          <InputNumber
            label="HP."
            value={editData.hp}
            onChange={newText => setEditData({...editData, hp: newText})}
          />

          <ButtonEdit onPress={updateProfile} text="Update Profil" />
        </>
      }
    />
  );
};

EditProfil.navigationOptions = ({navigation: {goBack}}) => ({
  headerTitle: drawer.label.editProfil,
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#fff"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
});

export default EditProfil;
