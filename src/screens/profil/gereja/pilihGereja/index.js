import React, {useState, useEffect} from 'react';

// Component
import {TouchableOpacity, ScrollView, Text, StatusBar} from 'react-native';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import warna from 'gsja/src/assets/colors';

// Function
import AsyncStorage from '@react-native-community/async-storage';

const PilihGereja = ({navigation}) => {
  const [listGereja, setListGereja] = useState(null);

  useEffect(() => {
    const getListGereja = async () => {
      const daftarGereja = await AsyncStorage.getItem('listGereja');
      const parsedData = JSON.parse(daftarGereja);

      setListGereja(parsedData);
    };

    getListGereja();
  }, []);

  const pilihGereja = item => {
    navigation.navigate('DetailProfilGereja', {selectedGereja: item});
  };

  return (
    <ScrollView>
      <StatusBar backgroundColor={warna.merahStatusbar} />

      {listGereja &&
        listGereja.map((item, key) => {
          return (
            <TouchableOpacity
              key={key}
              style={{
                marginHorizontal: 15,
                marginTop: 18,
                padding: 20,
                borderRadius: 5,
                borderColor: 'grey',
                borderWidth: 1.5,
              }}
              onPress={() => pilihGereja(item)}>
              <Text style={{color: warna.merahStatusbar, fontSize: 16}}>
                {item.name}
              </Text>
            </TouchableOpacity>
          );
        })}
    </ScrollView>
  );
};

PilihGereja.navigationOptions = ({navigation: {toggleDrawer}}) => ({
  headerTitle: 'Pilih Gereja',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="drawer"
        color={warna.putihAsli}
        iconName="nav-icon"
        onPress={toggleDrawer}
      />
    </NavHeaderButtons>
  ),
});

export default PilihGereja;
