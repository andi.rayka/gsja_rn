import React, {useState, useEffect} from 'react';

// Component
import DetailComponent, {
  TabelDataTwoRows,
} from 'gsja/src/components/container/DetailContainer';
import ButtonEdit from 'gsja/src/components/button/ButtonEditProfil';
import {Text} from 'react-native';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import warna from 'gsja/src/assets/colors';

// Function
import {apiPost} from 'gsja/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

const DetailProfilGereja = ({navigation: {getParam, navigate}}) => {
  const selectedGereja = getParam('selectedGereja');
  const [isLoading, setLoading] = useState(true);
  const [fetchedData, setFetchedData] = useState([]);

  // initial fetch detail Pendeta
  useEffect(() => {
    const initialFetch = async () => {
      const userData = await AsyncStorage.getItem('userData');

      let id_gereja = null;
      if (selectedGereja) {
        id_gereja = selectedGereja.idbranch;
      } else {
        id_gereja = JSON.parse(userData).idgereja;
      }

      let postData = [
        {
          url: '/getProfileGereja',
          data: {
            id_gereja,
          },
        },
      ];

      apiPost(postData)
        .then(resp => {
          // Karena gambar kalau linknya sama, nggak akan ganti.
          // Makanya diakali pakai param waktu untuk lihat gambar
          // Biar tiap buka refresh
          let waktu = moment().unix();
          resp[0].image += `?waktuInsert=${waktu}`;

          setFetchedData(resp[0]);
        })
        .then(() => setLoading(false));
    };

    initialFetch();
  }, []);

  const {tglulangtahun, rt, rw, iscity, u17, a17} = fetchedData;

  // Daftar Detail Data
  const detailData = [
    ['Kode Gereja', fetchedData.code],
    ['Status Gereja', fetchedData.status_sidang],
    ['Tanggal Berdiri', moment(tglulangtahun).format('DD MMMM YYYY')],
    ['Gembala Sidang', fetchedData.gembala_sidang_text],
    ['BPD Gereja', fetchedData.namabpd],
    ['Provinsi', fetchedData.namaprovinsi],
    ['Alamat', fetchedData.address],
    ['Kelurahan', fetchedData.kelurahan],
    ['Kecamatan', fetchedData.kecamatan],
    ['Kota', fetchedData.kota],
    ['Kode Pos', fetchedData.kodepos],
    ['RT / RW', rt && rw && `${rt} / ${rw}`],
    ['Email', fetchedData.email],
    ['Telp.', fetchedData.phone],
    ['Fax.', fetchedData.fax],
  ];

  // Jika bukan kota, maka ganti kelurahan dan kota dengan desa dan kabupaten
  if (iscity == 0) {
    detailData.splice(7, 1, ['Desa', fetchedData.desa]);
    detailData.splice(9, 1, ['Kabupaten', fetchedData.kabupaten]);
  }

  return (
    <DetailComponent
      isLoading={isLoading}
      imageURL={fetchedData.image}
      detailName={fetchedData.name}
      detailData={detailData}
      komponenDiBawah={
        <>
          <Text
            style={{
              color: warna.hitamText,
              fontSize: 17,
              fontFamily: 'Roboto-Medium',
            }}>
            Jadwal Gereja
          </Text>
          <Text
            style={{
              color: warna.hitamText,
              fontSize: 16,
              fontFamily: 'Roboto-Regular',
            }}>
            {fetchedData.jadwal ? fetchedData.jadwal : '-'}
          </Text>

          {/* Button Edit */}
          <ButtonEdit
            onPress={() =>
              navigate('EditProfilGereja', {profileData: fetchedData})
            }
            text="Edit Gereja"
          />
        </>
      }
      containerTambahan={
        <>
          <TabelDataTwoRows
            judul="Jumlah Jemaat"
            leftHead=""
            rightHead="Jumlah"
            contentData={[
              {kiri: 'Usia >= 17 tahun', kanan: a17 ? a17 : '-'},
              {kiri: 'Usia < 17 tahun', kanan: u17 ? u17 : '-'},
              {
                kiri: 'Total',
                kanan: a17 && u17 ? parseInt(u17, 10) + parseInt(a17, 10) : '-',
              },
            ]}
            leftKeyname="kiri"
            rightKeyname="kanan"
          />

          <Text
            style={{
              marginTop: 5,
              color: '#505050',
              fontSize: 16,
              fontFamily: 'Roboto-Medium',
            }}>
            Jumlah KKA : {fetchedData.jumlahkka}
          </Text>

          <TabelDataTwoRows
            judul="Pelayan Injil"
            leftHead="Nama"
            rightHead="Jabatan"
            contentData={fetchedData.listpi}
            leftKeyname="firstname"
            rightKeyname="titlename"
          />
        </>
      }
    />
  );
};

DetailProfilGereja.navigationOptions = ({navigation: {toggleDrawer}}) => ({
  headerTitle: 'Detail Gereja',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="drawer"
        color={warna.putihAsli}
        iconName="nav-icon"
        onPress={toggleDrawer}
      />
    </NavHeaderButtons>
  ),
});

export default DetailProfilGereja;
