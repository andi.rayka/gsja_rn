import React, {useState, useEffect} from 'react';

// Component
import {View, TouchableOpacity, Text} from 'react-native';
import MainContainer from 'gsja/src/components/container/ScrollviewContainer';
import {
  InputText,
  InputNumber,
  InputMultiLine,
  DateInput,
  ChooseIsKota,
  BigList,
} from 'gsja/src/components/form/EditProfil';
import ButtonEdit from 'gsja/src/components/button/ButtonEditProfil';
import {Image} from 'gsja/src/components/common';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import Icon from 'react-native-vector-icons/FontAwesome5';
import warna from 'gsja/src/assets/colors';
import rowStyles from 'gsja/src/assets/styles';

const {spaceBetweenRow} = rowStyles;

// Function
import {apiPost} from 'gsja/src/services/api';
// import ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import {StackActions, NavigationActions} from 'react-navigation';

const EditProfilGereja = ({navigation: {getParam, dispatch}}) => {
  const [editData, setEditData] = useState(getParam('profileData'));
  const [isLoading, setLoading] = useState(true);

  // Input data yang pakai list
  const [wilayah, setWilayah] = useState({
    idprovince: null,
    nameprovince: null,
  });

  // List Pilihan Filter
  const [listWilayah, setListWilayah] = useState([]);

  // initial fetch
  useEffect(() => {
    setLoading(true);
    let postData = [
      {
        url: '/getListPropinsi',
        data: {},
      },
    ];

    apiPost(postData).then(([resp]) => {
      setListWilayah(resp);

      const {id, name} = resp;
      setWilayah({id, name});

      // Set provinsi
      const {idprovince, namaprovinsi: nameprovince} = editData;
      setWilayah({
        idprovince: idprovince ? idprovince : null,
        nameprovince: nameprovince ? nameprovince : 'Pilih Provinsi',
      });
    });
    setLoading(false);
  }, [editData]);

  // Ketika buton update profile diklik
  const updateProfile = () => {
    setLoading(true);

    let postData = [
      {
        url: '/updateGereja',
        data: {
          id_gereja: editData.idbranch,
          nama: editData.name,
          tanggalperintisan: editData.tglulangtahun,
          alamat: editData.address,
          RT: editData.rt,
          RW: editData.rw,
          iscity: editData.iscity,
          desa: editData.desa,
          kota: editData.kota,
          kelurahan: editData.kelurahan,
          kecamatan: editData.kecamatan,
          kabupaten: editData.kabupaten,
          propinsi: wilayah.idprovince,
          telepon: editData.phone,
          fax: editData.fax,
          email: editData.email,
          kodepos: editData.kodepos,
          u17: editData.u17,
          a17: editData.a17,
          kka: editData.jumlahkka,
          jadwal: editData.jadwal,
          image: editData.base64Image,
        },
      },
    ];

    apiPost(postData).then(([resp]) => {
      if (resp.data) {
        alert(resp.data.error_message);
      } else {
        // Jika edit profil berhasil, kembali ke route stack awal, yaitu detail profil
        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({routeName: 'DetailProfilGereja'}),
          ],
        });

        dispatch(resetAction);

        alert('Berhasil update profil gereja');
      }
    });
    setLoading(false);
  };

  // Ganti Foto Profil
  const getImageFromGallery = () => {
    // ImagePicker.launchImageLibrary({mediaType: 'photo'}, resp => {
    //   const {uri, data, didCancel, error} = resp;

    //   if (didCancel || error) {
    //     return null;
    //   }

    //   setEditData({...editData, image: uri, base64Image: data});
    // });

    ImagePicker.openPicker({
      width: 768,
      height: 768,
      cropping: true,
      cropperCircleOverlay: true,
      freeStyleCropEnabled: true,
      avoidEmptySpaceAroundImage: true,
      includeBase64: true,
    })
      .then(resp => {
        const {path, data} = resp;
        setEditData({...editData, image: path, base64Image: data});
      })
      .catch(e => console.log('error img :', e));
  };

  return (
    <MainContainer
      isLoading={isLoading}
      children={
        <>
          {/* Foto Profil */}
          <View style={{width: 160, height: 160, alignSelf: 'center'}}>
            <View style={{borderRadius: 100, overflow: 'hidden'}}>
              <Image
                source={{uri: editData.image}}
                style={{width: 160, height: 160}}
              />
            </View>
            {/* Tombol Edit Foto  */}
            <TouchableOpacity
              onPress={getImageFromGallery}
              style={{
                backgroundColor: warna.merahHeader,
                padding: 9,
                borderRadius: 50,
                position: 'absolute',
                bottom: 0,
                right: 0,
              }}>
              <Icon name="pen" size={22} color="#fff" />
            </TouchableOpacity>
          </View>

          {/* Form Starts here */}
          {/* Nama  */}
          <InputText
            label="Nama"
            value={editData.name}
            onChange={newText => setEditData({...editData, name: newText})}
          />

          {/* Tanggal Perintisan */}
          <DateInput
            label="Tanggal Perintisan"
            selectedDate={editData.tglulangtahun}
            onSelect={newDate =>
              setEditData({...editData, tglulangtahun: newDate})
            }
          />

          {/* Alamat */}
          <InputText
            label="Alamat"
            value={editData.address}
            onChange={newText => setEditData({...editData, address: newText})}
          />

          {/* Pilih apakah kota atau non-kota */}
          <ChooseIsKota stateName={editData} setStateName={setEditData} />

          {/* Pilih Provinsi */}
          <BigList
            label="Provinsi"
            listOption={listWilayah}
            selectedList={wilayah}
            saveChange={setWilayah}
          />

          {/* Kode Pos */}
          <InputText
            label="Kode Pos"
            value={editData.kodepos}
            onChange={newText => setEditData({...editData, kodepos: newText})}
          />

          {/* RT/RW */}
          <View style={[{flex: 1}, spaceBetweenRow]}>
            {/* RT */}
            <InputText
              label="RT"
              value={editData.rt}
              onChange={newText => setEditData({...editData, rt: newText})}
            />

            {/* Garis miring pemisah */}
            <Text
              style={{
                paddingTop: 3,
                fontSize: 17,
                color: warna.hitamText,
                fontFamily: 'Roboto-Medium',
                marginHorizontal: 15,
              }}>
              /
            </Text>

            {/* RW */}
            <InputText
              label="RW"
              value={editData.rw}
              onChange={newText => setEditData({...editData, rw: newText})}
            />
          </View>

          {/* Jemaat 17 tahun ke atas */}
          <InputNumber
            label="Jumlah Jemaat >= 17 tahun"
            value={editData.a17}
            onChange={newNumber => setEditData({...editData, a17: newNumber})}
          />

          {/* Jemaat di bawah 17 tahun */}
          <InputNumber
            label="Jumlah Jemaat < 17 tahun"
            value={editData.u17}
            onChange={newNumber => setEditData({...editData, u17: newNumber})}
          />

          {/* Jumlah KKA */}
          <InputNumber
            label="Jumlah KKA"
            value={editData.jumlahkka}
            onChange={newNumber =>
              setEditData({...editData, jumlahkka: newNumber})
            }
          />

          {/* Email */}
          <InputText
            label="Email"
            value={editData.email}
            onChange={newText => setEditData({...editData, email: newText})}
          />

          {/* Phone */}
          <InputText
            label="Telp."
            value={editData.phone}
            onChange={newText => setEditData({...editData, phone: newText})}
          />

          {/* Fax */}
          <InputText
            label="Fax."
            value={editData.fax}
            onChange={newText => setEditData({...editData, fax: newText})}
          />

          {/* Jadwal */}
          <InputMultiLine
            label="Jadwal Gereja"
            value={editData.jadwal}
            onChange={newText => setEditData({...editData, jadwal: newText})}
          />

          <ButtonEdit onPress={updateProfile} text="Update Gereja" />
        </>
      }
    />
  );
};

EditProfilGereja.navigationOptions = ({navigation: {goBack}}) => ({
  headerTitle: 'Edit Gereja',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#fff"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
});

export default EditProfilGereja;
