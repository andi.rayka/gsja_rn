import React from 'react';

// Component
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Image} from 'gsja/src/components/common';

// Micro Component
import Icon from 'react-native-vector-icons/Fontisto';
import rowStyles from 'gsja/src/assets/styles';

const {flexStartRow} = rowStyles;

// Function
import {withNavigation} from 'react-navigation';

const ListInstitusiItem = ({item, navigation: {navigate}}) => {
  return (
    // Container Utama
    <TouchableOpacity
      activeOpacity={0.6}
      style={[
        flexStartRow,
        {
          backgroundColor: '#fff',
          paddingHorizontal: 10,
          paddingVertical: 9,
          elevation: 3,
        },
      ]}
      onPress={() =>
        navigate('DetailInstitusi', {id_institusi: item.idbranch})
      }>
      {/* Foto institusi */}
      <Image
        source={{uri: item.image}}
        style={{width: 100, height: 100, marginBottom: 51}}
      />

      {/* Bagian data institusi */}
      <View style={{marginLeft: 12, flex: 1}}>
        {/* Nama institusi */}
        <Text
          style={{
            color: '#303030',
            fontSize: 16,
            fontFamily: 'Roboto-Medium',
          }}>
          {item.name}
        </Text>

        {/* Nama gembala sidang/direksi  */}
        <Text style={[styles.textBiodata, {lineHeight: 18, marginTop: 4}]}>
          Direksi: {item.gembala_sidang_text}
        </Text>
        {/* Wilayah/BPD gereja  */}
        <Text style={[styles.textBiodata, {lineHeight: 18, marginTop: 5}]}>
          BPD: {item.wilayah}
        </Text>
        {/* BPW gereja  */}
        <Text style={[styles.textBiodata, {lineHeight: 18}]}>
          BPW: {item.bpw}
        </Text>
        {/* provinsi gereja  */}
        <Text style={[styles.textBiodata, {lineHeight: 18}]}>
          Provinsi: {item.nameprovince}
        </Text>

        {/* Alamat Gereja  */}
        <Text style={[styles.textBiodata, {marginTop: 8, marginBottom: 6}]}>
          {item.address ? item.address : '-'}
        </Text>

        {/* Kontak Gereja */}
        <View style={flexStartRow}>
          <Icon name="phone" size={16} />
          <Text style={[styles.textBiodata, {marginLeft: 8}]}>
            {item.hp ? item.hp : '-'}
          </Text>
        </View>
        {/* Email gereja  */}
        <View style={flexStartRow}>
          <Icon name="email" size={16} />
          <Text style={[styles.textBiodata, {marginLeft: 8}]}>
            {item.email ? item.email : '-'}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

// Separator/Pembatas antar item
export const itemSeparator = () => (
  <View style={{backgroundColor: '#e9e9e9', height: 11, width: '100%'}} />
);

const styles = StyleSheet.create({
  textBiodata: {
    color: '#303030',
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
  },
});

export default withNavigation(ListInstitusiItem);
