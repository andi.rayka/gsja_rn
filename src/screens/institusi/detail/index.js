import React, {useState, useEffect, useRef} from 'react';

// Component
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Modal,
  Linking,
  Dimensions,
} from 'react-native';
import DetailComponent, {
  TabelDataTwoRows,
} from 'gsja/src/components/container/DetailContainer';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import IconAwesome from 'react-native-vector-icons/FontAwesome5';
import warna from 'gsja/src/assets/colors';

// Function
import {apiPost} from 'gsja/src/services/api';
import moment from 'moment';

const DetailInstitusi = ({navigation: {getParam, setParams, state}}) => {
  const [isLoading, setLoading] = useState(true);
  const [fetchedData, setFetchedData] = useState([]);
  const [isModalOpen, toggleOpenModal] = useState(false);

  // Menghubungkan button di header dengan function di dlm komponen
  const {params = {}} = state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);
  // Set button with function
  useEffect(() => {
    setParam.current({openModal: () => toggleOpenModal(true)});
  }, [paramDesc, setParam]);

  // initial fetch detail institusi
  useEffect(() => {
    const id_institusi = getParam('id_institusi');

    let postData = [
      {
        url: '/getDetailInstitusi',
        data: {
          id_institusi,
        },
      },
    ];

    apiPost(postData)
      .then(resp => setFetchedData(resp[0]))
      .then(() => setLoading(false));
  }, [getParam]);

  // Handle search lokasi dengan aplikasi google map
  const searchLocationByGMap = (lat_long, plainSearchParam) => {
    // const searchParam = plainSearchParam.split(' ').join('+');
    const searchParam = lat_long
      ? lat_long.replace('_', ',')
      : plainSearchParam.split(' ').join('+');
    let searchUrl = `https://www.google.com/maps/search/?api=1&query=${searchParam}`;

    Linking.canOpenURL(searchUrl)
      .then(supported => {
        if (!supported) {
          console.log('Gagal handle url: ' + searchUrl);
        } else {
          return Linking.openURL(searchUrl);
        }
      })
      .catch(err => console.error('An error occurred', err));
  };

  // Handle get direction lokasi dengan aplikasi google map
  // Param -> latitude dan longitude, tempat yang dicari
  const getLocationDirection = (lat_long, plainUrlParam) => {
    // Jika latitude dan longitude adalah null, maka cari berdasarkan nama
    const directionParam = lat_long
      ? lat_long.replace('_', ',')
      : plainUrlParam.split(' ').join('+');

    const directionUrl = `https://www.google.com/maps/dir/?api=1&destination=${directionParam}`;

    Linking.canOpenURL(directionUrl)
      .then(supported => {
        if (!supported) {
          console.log("Can't handle url: " + directionUrl);
        }
        return Linking.openURL(directionUrl);
      })
      .catch(err => console.error('An error occurred', err));
  };

  // Modal Untuk Akses Goggle Maps
  const ModalLocation = () => {
    return (
      <Modal
        animationType="fade"
        transparent
        visible={isModalOpen}
        onRequestClose={() => toggleOpenModal(false)}>
        {/* Space kosong di seluruh halaman/selain content modal  */}
        <TouchableOpacity
          onPress={() => toggleOpenModal(false)}
          style={styles.containerFreespace}
        />

        {/* Konten modal  */}
        <View
          style={{
            flexDirection: 'row',
            width: Dimensions.get('window').width,
            backgroundColor: warna.putihAsli,
          }}>
          {/* Tunjukkan arah/direction gereja */}
          <TouchableOpacity
            onPress={() =>
              getLocationDirection(fetchedData.location, fetchedData.name)
            }
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 20,
              paddingBottom: 16,
            }}>
            <IconAwesome
              name="directions"
              size={37}
              color={warna.merahHeader}
            />
            <Text
              style={{
                color: warna.hitamText,
                fontSize: 14,
                fontFamily: 'Roboto-Regular',
              }}>
              Tunjukkan Arah
            </Text>
          </TouchableOpacity>

          {/* Search gereja di google map */}
          <TouchableOpacity
            onPress={() =>
              searchLocationByGMap(fetchedData.location, fetchedData.name)
            }
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 20,
              paddingBottom: 16,
            }}>
            <IconAwesome
              name="map-marker-alt"
              size={37}
              color={warna.merahHeader}
            />
            <Text
              style={{
                color: warna.hitamText,
                fontSize: 14,
                fontFamily: 'Roboto-Regular',
              }}>
              Tampilkan Peta
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  const {tglulangtahun, rt, rw} = fetchedData;

  // Daftar Detail Data
  const detailData = [
    ['Kode Institusi', fetchedData.code],
    ['Tanggal Berdiri', moment(tglulangtahun).format('DD MMMM YYYY')],
    ['Direksi', fetchedData.gembala_sidang_text],
    ['BPD', fetchedData.namabpd],
    ['BPW', fetchedData.namabpw],
    ['Provinsi', fetchedData.namaprovinsi],
    ['Alamat', fetchedData.address],
    ['RT / RW', rt && rw && `${rt} / ${rw}`],
    ['Email', fetchedData.email],
    ['Telp.', fetchedData.phone],
    ['HP.', fetchedData.hp],
    ['Fax.', fetchedData.fax],
  ];

  return (
    <DetailComponent
      isLoading={isLoading}
      imageURL={fetchedData.image}
      detailName={fetchedData.name}
      detailData={detailData}
      komponenDiBawah={<ModalLocation />}
      containerTambahan={
        <TabelDataTwoRows
          judul="Pelayan Injil"
          leftHead="Nama"
          rightHead="Jabatan"
          contentData={fetchedData.listpi}
          leftKeyname="firstname"
          rightKeyname="titlename"
        />
      }
    />
  );
};

DetailInstitusi.navigationOptions = ({navigation: {goBack, getParam}}) => ({
  headerTitle: 'Detail Institusi',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color={warna.putihAsli}
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="map"
        color={warna.putihAsli}
        iconName="map-marker-alt"
        onPress={getParam('openModal')}
      />
    </NavHeaderButtons>
  ),
});

const styles = StyleSheet.create({
  containerFreespace: {
    flex: 1,
    backgroundColor: warna.hitamAsli,
    opacity: 0.6,
  },
});

export default DetailInstitusi;
