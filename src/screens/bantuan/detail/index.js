import React, {useState, useEffect} from 'react';

// Component
import {View, Text} from 'react-native';
import MainContainer from 'gsja/src/components/container/ScrollviewContainer';
import {Image} from 'gsja/src/components/common';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import Icon from 'react-native-vector-icons/Fontisto';
import rowStyles from 'gsja/src/assets/styles';
import warna from 'gsja/src/assets/colors';

const {flexStartRow} = rowStyles;

// Function
import {apiPost} from 'gsja/src/services/api';

const DetailBantuan = ({navigation: {getParam}}) => {
  const [isLoading, setLoading] = useState(true);
  const [fetchedData, setFetchedData] = useState([]);

  // getParam dari navigate yg membawa ke halaman ini
  const id_komunitas = getParam('id_komunitas');

  // Initial fetch
  useEffect(() => {
    setLoading(true);

    let postData = [
      {
        url: '/getDetailBantuan',
        data: {
          id_komunitas,
        },
      },
    ];

    apiPost(postData)
      .then(resp => {
        setFetchedData(resp[0]);
      })
      .then(() => setLoading(false));
  }, [id_komunitas]);

  return (
    <MainContainer
      isLoading={isLoading}
      children={
        <>
          {/* Bagian atas / judul event  */}
          <View style={flexStartRow}>
            {/* Foto Profil Pembuat Postingan */}
            <View
              style={{
                height: 45,
                width: 45,
                borderRadius: 45,
                overflow: 'hidden',
              }}>
              <Image
                source={{uri: fetchedData.fotoprofile}}
                style={{height: 45, width: 45, borderRadius: 45}}
              />
            </View>

            <View style={{flex: 1, marginLeft: 12}}>
              {/* Text Judul  */}
              <Text
                style={{
                  color: warna.hitamText2,
                  fontSize: 18,
                  fontFamily: 'Roboto-Medium',
                }}>
                {fetchedData.title}
              </Text>

              {/* Text Pembuat Postingan */}
              <View style={[flexStartRow, {marginTop: 5}]}>
                <Icon name="person" size={16} />
                <Text
                  style={{
                    flex: 1,
                    color: warna.hitamText2,
                    fontSize: 14,
                    fontFamily: 'Roboto-Regular',
                    marginLeft: 5,
                  }}>
                  {fetchedData.firstname} - {fetchedData.name}
                </Text>
              </View>
            </View>
          </View>

          {/* Bagian Informasi Konten */}
          <Text
            style={{
              color: warna.hitamText2,
              fontSize: 15,
              fontFamily: 'Roboto-Regular',
              marginTop: 13,
              lineHeight: 22,
            }}>
            {fetchedData.info}
          </Text>

          {/* Gambar/Image informasi  */}
          {fetchedData.image && (
            <Image
              source={{uri: fetchedData.image}}
              resizeMode="cover"
              style={{height: 200, width: '100%', marginTop: 15}}
            />
          )}
        </>
      }
    />
  );
};

DetailBantuan.navigationOptions = ({navigation: {goBack}}) => {
  return {
    headerTitle: 'Detail Bantuan',
    headerLeft: (
      <NavHeaderButtons>
        <Item
          title="back"
          color="#fff"
          iconName="arrow-left"
          onPress={() => goBack()}
        />
      </NavHeaderButtons>
    ),
  };
};

export default DetailBantuan;
