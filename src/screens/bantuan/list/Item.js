import React from 'react';

// Component
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Image} from 'gsja/src/components/common';

// Micro Component
import Icon from 'react-native-vector-icons/Fontisto';
import rowStyles from 'gsja/src/assets/styles';
import warna from 'gsja/src/assets/colors';

const {spaceBetweenRow} = rowStyles;

// Function
import {withNavigation} from 'react-navigation';

const ListEventItem = ({item, navigation: {navigate}}) => {
  return (
    // Container Utama
    <View
      activeOpacity={0.6}
      style={{
        backgroundColor: warna.putihAsli,
        paddingHorizontal: 11,
        paddingTop: 14,
      }}>
      {/* Bagian atas / judul postingan */}
      <View>
        {/* Text Judul */}
        <TouchableOpacity
          onPress={() => navigate('DetailPostingan', {id_komunitas: item.idv})}
          style={{marginRight: 50}}>
          <Text
            style={{
              color: warna.hitamText2,
              fontSize: 18,
              fontFamily: 'Roboto-Medium',
            }}>
            {item.title}
          </Text>
        </TouchableOpacity>

        {/* Text Nama Pembuat post */}
        <View style={[spaceBetweenRow, {marginTop: 5}]}>
          <Icon name="person" size={16} />
          <Text style={styles.textPembuatPost}>
            {`${item.firstname} | ${item.name}`}
          </Text>
        </View>
      </View>

      {/* Bagian Informasi Konten */}
      <Text
        numberOfLines={4}
        ellipsizeMode="tail"
        style={styles.textInfoContent}>
        {item.info}
      </Text>

      <ButtonBacaSelanjutnya
        onPress={() =>
          navigate('DetailBantuan', {id_komunitas: item.idcommunitypost})
        }
      />

      {/* Gambar/Image informasi  */}
      {item.image && (
        <Image
          source={{uri: item.image}}
          resizeMode="cover"
          style={{height: 200, width: '100%', marginTop: 14}}
        />
      )}
    </View>
  );
};

// Separator/Pembatas antar item
export const itemSeparator = () => (
  <View style={{backgroundColor: warna.abuabu1, height: 11, width: '100%'}} />
);

// komen
const ButtonBacaSelanjutnya = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text
        style={{
          color: warna.biruLink,
          fontSize: 15,
          fontFamily: 'Roboto-Regular',
        }}>
        Baca Selanjutnya ...
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textInfoContent: {
    color: warna.hitamText2,
    fontSize: 15,
    fontFamily: 'Roboto-Regular',
    marginTop: 13,
    lineHeight: 22,
  },
  textPembuatPost: {
    flex: 1,
    color: warna.hitamText2,
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
    marginLeft: 5,
  },
});

export default withNavigation(ListEventItem);
