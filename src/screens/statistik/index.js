import React, {useState, useEffect, useRef} from 'react';

// Component
import MainContainer from 'gsja/src/components/container/ScrollviewContainer';
import ModalFilter, {
  ButtonList,
} from 'gsja/src/components/filterData/ModalFilter';
import {Text, View} from 'react-native';
import {PieChart} from 'react-native-svg-charts';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import strings from 'gsja/src/assets/strings';
import rowStyles from 'gsja/src/assets/styles';

const {drawer} = strings;
const {flexStartRow} = rowStyles;

// Function
import {apiPost} from 'gsja/src/services/api';

const Statistik = ({navigation: {state, setParams}}) => {
  const [isLoading, setIsLoading] = useState(true);

  // Input Filter
  const [postFilter, setPostFilter] = useState({
    wilayah: {id: null, name: null},
  });

  // List Pilihan Filter
  const [listWilayah, setListWilayah] = useState([]);

  // List data statistik
  const [statData, setStatData] = useState({});
  const [jmlData, setJmlData] = useState({});
  const [keterangan, setKeterangan] = useState({});

  // Modal open
  const [isModalFilterOpen, toggleOpenModalFilter] = useState(false);

  // Menghubungkan button di header dengan function di dlm komponen
  const {params = {}} = state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);
  // Bind button with function
  useEffect(() => {
    setParam.current({openModalFilter: () => toggleOpenModalFilter(true)});
  }, [paramDesc, setParam]);

  // initial fetch list filter
  useEffect(() => {
    let postData = [
      {
        url: '/getListWilayah',
        data: {},
      },
    ];

    apiPost(postData).then(resp => setListWilayah(resp[0]));
  }, []);

  // Fetch data statistik
  useEffect(() => {
    setIsLoading(true);
    let postData = [
      {
        url: '/getStatistik',
        data: {
          wilayah: postFilter.wilayah.id,
        },
      },
    ];

    apiPost(postData)
      .then(resp => {
        const {jml, gereja, pi, jemaat} = resp[0];

        // Data angka dan warna chart untuk statData
        // Warna, nama dan angka untuk keterangan

        // Gereja
        let dataGereja = [
          {key: 1, amount: gereja.jml_pembina, svg: {fill: '#f44336'}},
          {key: 2, amount: gereja.jml_madya, svg: {fill: '#3f51b5'}},
          {key: 3, amount: gereja.jml_pratama, svg: {fill: '#7cb342'}},
          {key: 4, amount: gereja.jml_perintisan, svg: {fill: '#ff9800'}},
        ];
        let ketGereja = [
          {color: '#f44336', text: 'Pembina', number: gereja.jml_pembina, persen: gereja.prsn_pembina},
          {color: '#3f51b5', text: 'Madya', number: gereja.jml_madya, persen: gereja.prsn_madya},
          {color: '#7cb342', text: 'Pratama', number: gereja.jml_pratama, persen: gereja.prsn_pratama},
          {color: '#ff9800', text: 'Perintisan', number: gereja.jml_perintisan, persen: gereja.prsn_perintisan},
        ];

        // Pelayan Injil
        let dataPi = [
          {key: 1, amount: pi.jml_pdt, svg: {fill: '#f44336'}},
          {key: 2, amount: pi.jml_pdm, svg: {fill: '#3f51b5'}},
          {key: 3, amount: pi.jml_pdp, svg: {fill: '#7cb342'}},
          {key: 4, amount: pi.jml_pi, svg: {fill: '#ff9800'}},
        ];
        let ketPi = [
          {color: '#f44336', text: 'Pdt', number: pi.jml_pdt, persen: pi.prsn_pdt},
          {color: '#3f51b5', text: 'Pdm', number: pi.jml_pdm, persen: pi.prsn_pdm},
          {color: '#7cb342', text: 'Pdp', number: pi.jml_pdp, persen: pi.prsn_pdp},
          {color: '#ff9800', text: 'SK', number: pi.jml_pi, persen: pi.prsn_pi},
        ];

        // Jemaat
        let dataJemaat = [
          {key: 2, amount: jemaat.jml_a17, svg: {fill: '#3f51b5'}},
          {key: 1, amount: jemaat.jml_u17, svg: {fill: '#f44336'}},
        ];
        let ketJemaat = [
          {color: '#3f51b5', text: 'Usia >= 17 tahun', number: jemaat.jml_a17, persen: jemaat.prsn_a17},
          {color: '#f44336', text: 'Usia < 17 tahun', number: jemaat.jml_u17, persen: jemaat.prsn_u17},
        ];

        // Set data ke state
        setStatData({
          gereja: dataGereja,
          pi: dataPi,
          jemaat: dataJemaat,
        });

        setJmlData({
          gereja: jml.jml_gereja,
          pi: jml.jml_pendeta,
          jemaat: jml.jml_jemaat,
        });

        setKeterangan({
          gereja: ketGereja,
          pi: ketPi,
          jemaat: ketJemaat,
        });
      })
      .then(() => setIsLoading(false));
  }, [postFilter]);

  return (
    <MainContainer
      noPaddingHorizontal
      isLoading={isLoading}
      children={
        <>
          <ViewChart
            judul="Pelayan Injil"
            data={statData.pi}
            total={jmlData.pi}
            arrKeterangan={keterangan.pi}
          />

          <SeparatorChart />

          <ViewChart
            judul="Gereja"
            data={statData.gereja}
            total={jmlData.gereja}
            arrKeterangan={keterangan.gereja}
          />

          <SeparatorChart />

          <ViewChart
            judul="Jemaat"
            data={statData.jemaat}
            total={jmlData.jemaat}
            arrKeterangan={keterangan.jemaat}
          />
        </>
      }
      loadingScreenException={
        <ModalFilter
          isModalOpen={isModalFilterOpen}
          onClose={() => toggleOpenModalFilter(false)}
          onResetFilter={() =>
            setPostFilter({
              ...postFilter,
              wilayah: {id: null, name: null},
            })
          }
          title="Filter Statistik"
          child={
            <ButtonList
              defaultValueisIndex0
              title="Wilayah"
              selectedList={postFilter.wilayah}
              listOption={listWilayah}
              saveChange={obj => setPostFilter({...postFilter, wilayah: obj})}
              idName="idorganization"
            />
          }
        />
      }
    />
  );
};

// Pembatas antar viewchart
const SeparatorChart = () => (
  <View
    style={{
      height: 5,
      width: '100%',
      backgroundColor: '#e9e9e9',
      marginVertical: 16,
    }}
  />
);

// Komponen berisi chart dan keterangannya
const ViewChart = ({judul, total, data, arrKeterangan}) => {
  return (
    <View style={{paddingHorizontal: 11}}>
      {/* Judul Chart */}
      <Text
        style={{
          color: '#303030',
          fontSize: 25,
          fontFamily: 'Roboto-Medium',
          textAlign: 'center',
          marginBottom: 3,
        }}>
        {`Statistik ${judul}`}
      </Text>

      {/* Total data */}
      <Text
        style={{
          color: '#505050',
          fontSize: 18,
          fontFamily: 'Roboto-Medium',
          textAlign: 'center',
          marginBottom: 15,
        }}>
        {`Total ${total} ${judul}`}
      </Text>

      {/* Komponen Chart */}
      <PieChart
        style={{height: 220}}
        valueAccessor={({item}) => item.amount}
        data={data}
        outerRadius={'96%'}
      />

      {/* Keterangan Chart  */}
      {arrKeterangan.map((item, key) => {
        return (
          <View
            key={key}
            style={[
              flexStartRow,
              {justifyContent: 'space-between', marginTop: 12},
            ]}>
            <View style={flexStartRow}>
              {/* Warna di sebelah kiri */}
              <Text
                style={{
                  // masih bingung gimana buat persen
                  color: '#fff',
                  // color: item.color,
                  fontSize: 13,
                  fontFamily: 'Poppins-Regular',
                  paddingHorizontal: 10,
                  paddingVertical: 4,
                  backgroundColor: item.color,
                  borderRadius: 3,
                }}>
                {item.persen}%
              </Text>
              {/* Text keterangan */}
              <Text
                style={{
                  color: '#303030',
                  fontSize: 16,
                  fontFamily: 'Roboto-Medium',
                  marginLeft: 10,
                }}>
                {item.text}
              </Text>
            </View>

            {/* Angka data */}
            <Text
              style={{
                color: '#5e0101',
                fontSize: 18,
                fontFamily: 'Roboto-Medium',
              }}>
              {item.number}
            </Text>
          </View>
        );
      })}
    </View>
  );
};

Statistik.navigationOptions = ({navigation: {toggleDrawer, getParam}}) => ({
  headerTitle: drawer.label.statistik,
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="drawer"
        color="#fff"
        iconName="nav-icon"
        onPress={toggleDrawer}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="filter"
        color="#fff"
        iconName="filter"
        onPress={getParam('openModalFilter')}
      />
    </NavHeaderButtons>
  ),
});

export default Statistik;
