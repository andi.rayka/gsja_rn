import React, {Component} from 'react';
import {StyleSheet, View, ScrollView, Text} from 'react-native';
import {Table, Row, Rows} from 'react-native-table-component';

export default class ExampleThree extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: [
        'Head',
        'Head2',
        'Head3',
        'Head4',
        'Head5',
        'Head6',
        'Head7',
        'Head8',
        'Head9',
      ],
      widthArr: [40, 60, 80, 100, 120, 140, 160, 180, 200],
    };
  }

  render() {
    const state = this.state;
    const tableData = [];
    for (let i = 0; i < 30; i += 1) {
      const rowData = [];
      for (let j = 0; j < 9; j += 1) {
        rowData.push(`${i}${j}`);
      }
      tableData.push(rowData);
    }

    return (
      <ScrollView contentContainerStyle={{flexGrow: 1, padding: 9}}>
        <Text>Judul</Text>

        <ScrollView horizontal>
          <Table>
            <Row data={state.tableHead} />
            <Rows data={tableData} />
          </Table>
        </ScrollView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff'},
  header: {height: 50, backgroundColor: '#537791'},
  text: {textAlign: 'center', fontWeight: '100'},
  dataWrapper: {marginTop: -1},
  row: {height: 40, backgroundColor: '#E7E6E1'},
});
