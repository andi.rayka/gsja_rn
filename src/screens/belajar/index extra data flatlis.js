import React, {useState, useEffect} from 'react';

// Component
import {View, Text, TouchableOpacity, Button, FlatList} from 'react-native';

// Micro Component
import warna from 'gsja/src/assets/colors';

// Function
import {apiPost} from 'gsja/src/services/api';

const BelajarHome = () => {
  const [fetchedData, setFetchedData] = useState([
    'hala',
    'ia',
    'adsf',
    'adsf',
    'asdf',
  ]);
  const [ganti, setGanti] = useState(['ganti']);
  const [isRefreshing, setIsRefreshing] = useState(false);

  return (
    <>
      <FlatList
        data="https://reqres.in/api/users?page=1"
        renderItem={({item}) => <Text>{item}</Text>}
        keyExtractor={(item, key) => key.toString()}
        extraData={ganti}
        refreshing={isRefreshing}
        onRefresh={() => {
          setIsRefreshing(true);
        }}
      />

      <Button
        onPress={() => {
          // setGanti([...ganti, 'terbaru']);
          setIsRefreshing(false);
        }}
        title="ganti"
      />
    </>
  );
};

export default BelajarHome;
