import React, {useState, useEffect} from 'react';

// Component
import {View, Text, StyleSheet} from 'react-native';
import MainContainer from 'gsja/src/components/container/ScrollviewContainer';
import {Image} from 'gsja/src/components/common';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import Icon from 'react-native-vector-icons/Fontisto';
import warna from 'gsja/src/assets/colors';
import rowStyles from 'gsja/src/assets/styles';

const {spaceBetweenRow} = rowStyles;

// Functions
import {apiPost} from 'gsja/src/services/api';
import moment from 'moment';

// return tampilan tanggal kepada user
const parseDate = fetchedDate => {
  return {
    tgl: moment(fetchedDate).date(),
    bulan: moment(fetchedDate).format('MMM'),
    tahun: moment(fetchedDate).year(),
  };
};

const DetailKegiatan = ({navigation: {getParam}}) => {
  const [isLoading, setLoading] = useState(true);
  const [fetchedData, setFetchedData] = useState([]);

  const {textDateStart} = styles;

  // initial fetch detail Pendeta
  useEffect(() => {
    setLoading(true);

    const id_kegiatan = getParam('id_kegiatan');
    const id_notification = getParam('id_notification');
    let postData = [
      {
        url: '/getDetailKegiatan',
        data: {
          id_kegiatan,
          id_notification,
        },
      },
    ];

    apiPost(postData)
      .then(resp => {
        setFetchedData(resp[0]);
      })
      .then(() => setLoading(false));
  }, [getParam]);

  return (
    <MainContainer
      isLoading={isLoading}
      children={
        <>
          {/* Bagian atas / judul event  */}
          <View style={spaceBetweenRow}>
            <View style={{flex: 1, marginLeft: 4}}>
              {/* Text Judul  */}
              <Text
                style={{
                  color: warna.hitamText2,
                  fontSize: 18,
                  fontFamily: 'Roboto-Medium',
                }}>
                {fetchedData.name}
              </Text>

              {/* Text Nama Organisasi */}
              <View style={[spaceBetweenRow, {marginTop: 5}]}>
                <Icon name="person" size={16} />
                <Text
                  style={{
                    flex: 1,
                    color: warna.hitamText2,
                    fontSize: 14,
                    fontFamily: 'Roboto-Regular',
                    marginLeft: 5,
                  }}>
                  {fetchedData.namaorganisasi}
                </Text>
              </View>
            </View>
          </View>

          {/* Bagian Keterangan Acara  */}
          <View
            style={[
              spaceBetweenRow,
              {
                paddingTop: 7,
                paddingHorizontal: 9,
                paddingBottom: 4,
                marginTop: 14,
                borderRadius: 5,
                elevation: 2,
              },
            ]}>
            {/* Tanggal acara  */}
            <View
              style={{
                backgroundColor: warna.merahStatusbar,
                borderRadius: 3,
                paddingHorizontal: 15,
                paddingVertical: 5,
              }}>
              <Text style={[textDateStart, {fontSize: 23, lineHeight: 28}]}>
                {parseDate(fetchedData.datestart).tgl}
              </Text>
              <Text style={[textDateStart, {fontSize: 16}]}>
                {parseDate(fetchedData.datestart).bulan}
              </Text>
              <Text style={[textDateStart, {fontSize: 16}]}>
                {parseDate(fetchedData.datestart).tahun}
              </Text>
            </View>

            {/* Tempat Dan waktu acara  */}
            <View
              style={{
                flex: 1,
                marginLeft: 12,
              }}>
              <View style={spaceBetweenRow}>
                <Icon name="map-marker-alt" size={17} />
                <Text style={styles.textPlaceTime}>{fetchedData.place}</Text>
              </View>
              <View style={spaceBetweenRow}>
                <Icon name="clock" size={17} />
                <Text style={styles.textPlaceTime}>
                  {`${fetchedData.timestart} - ${
                    fetchedData.timeend ? fetchedData.timeend : 'selesai'
                  }`}
                </Text>
              </View>
              <View style={spaceBetweenRow}>
                <Icon name="hourglass-half" size={17} />
                <Text style={styles.textPlaceTime}>
                  {`Durasi ${fetchedData.days} hari`}
                </Text>
              </View>
            </View>
          </View>

          {/* Informasi kegiatan */}
          <Text
            style={{
              color: warna.hitamText2,
              fontSize: 16,
              fontFamily: 'Roboto-Regular',
              marginTop: 15,
            }}>
            {fetchedData.info}
          </Text>

          {/* Gambar/Image informasi  */}
          {fetchedData.images && (
            <Image
              source={{uri: fetchedData.images}}
              resizeMode="cover"
              style={{height: 200, width: '100%', marginTop: 25}}
            />
          )}
        </>
      }
    />
  );
};

DetailKegiatan.navigationOptions = ({navigation: {goBack}}) => ({
  headerTitle: 'Detail Kegiatan',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#fff"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
});

const styles = StyleSheet.create({
  textDateStart: {
    color: warna.putihAsli,
    textAlign: 'center',
    fontFamily: 'Poppins-Medium',
    lineHeight: 22,
  },
  textPlaceTime: {
    flex: 1,
    color: warna.hitamText,
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    marginLeft: 9,
    marginVertical: 4,
  },
});

export default DetailKegiatan;
