import React, {useState} from 'react';

// Component
import {
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Text,
  Dimensions,
} from 'react-native';
import {OptionListPostingan} from '../../list/Component';
import {Image} from 'gsja/src/components/common';

// Micro Component
import Icon from 'react-native-vector-icons/Fontisto';
import rowStyles from 'gsja/src/assets/styles';
import warna from 'gsja/src/assets/colors';

const {flexStartRow} = rowStyles;

// Function
import moment from 'moment';

export const ListKomentar = ({
  // info komentar induk
  image,
  name,
  info,
  createdAt,

  // Komentar induk
  onPressReply,
  candelete,
  onPressDelete,
  onPressReport,

  // Komentar Reply
  replyList,
  deleteReply,
  reportReply,
}) => {
  const [isOptionOpen, setIsOptionOpen] = useState(false);

  return (
    <>
      <View style={[flexStartRow, {marginTop: 15}]}>
        {/* Pilihan menu di tiap list komentar */}
        {isOptionOpen && (
          <TouchableWithoutFeedback onPress={() => setIsOptionOpen(false)}>
            <View
              style={{
                position: 'absolute',
                width: Dimensions.get('window').width - 18,
                height: '100%',
                zIndex: 2,
              }}>
              <View
                style={{
                  backgroundColor: warna.putihAsli,
                  marginRight: 20,
                  alignSelf: 'flex-end',
                  elevation: 5,
                  paddingHorizontal: 17,
                  paddingVertical: 3,
                  borderRadius: 2,
                }}>
                {candelete === 1 ? (
                  <OptionListPostingan type="delete" onPress={onPressDelete} />
                ) : (
                  <OptionListPostingan type="report" onPress={onPressReport} />
                )}
              </View>
            </View>
          </TouchableWithoutFeedback>
        )}

        {/* Foto profil */}
        <View
          style={{height: 37, width: 37, borderRadius: 37, overflow: 'hidden'}}>
          <Image
            source={{uri: image}}
            style={{height: 37, width: 37, borderRadius: 37}}
          />
        </View>

        {/* Nama dan komentar */}
        <View style={{marginLeft: 10, flex: 1}}>
          <Text
            style={{
              color: warna.hitamText2,
              fontSize: 15,
              fontFamily: 'Roboto-Bold',
              marginRight: 11,
            }}>
            {name}
            <Text
              style={{
                fontFamily: 'Roboto-Regular',
                fontWeight: 'normal',
                lineHeight: 18,
              }}>
              {' ' + info}
            </Text>
          </Text>

          {/* Option Komentar */}
          <TouchableOpacity
            onPress={() => setIsOptionOpen(true)}
            style={{
              position: 'absolute',
              right: 0,
              top: 3,
              paddingBottom: 10,
              paddingLeft: 12,
            }}>
            <Icon name="more-v-a" size={17} color={warna.hitamText2} />
          </TouchableOpacity>

          {/* Waktu Posting komentar */}
          <View style={flexStartRow}>
            <Text
              style={{
                color: 'grey',
                fontSize: 11,
                fontFamily: 'Roboto-Regular',
              }}>
              {moment(createdAt).toNow(true)}
            </Text>

            <TouchableOpacity onPress={onPressReply}>
              <Text
                style={{
                  color: 'grey',
                  fontSize: 13,
                  fontFamily: 'Roboto-Medium',
                  marginLeft: 10,
                }}>
                Balas
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      {/* Komentar balasan jika ada */}
      {replyList.map((item, key) => {
        return (
          <NestedKomentar
            key={key}
            name={item.firstname}
            info={item.info}
            createdAt={item.t_inserttime}
            candelete={item.candelete}
            onPressDelete={() => deleteReply(item.idcommunitypost)}
            onPressReport={() => reportReply(item.idcommunitypost)}
          />
        );
      })}
    </>
  );
};

const NestedKomentar = ({
  name,
  info,
  createdAt,
  candelete,
  onPressDelete,
  onPressReport,
}) => {
  const [isOptionOpen, setIsOptionOpen] = useState(false);

  return (
    <View style={{marginTop: 15, paddingLeft: 45}}>
      {/* Pilihan menu di tiap list komentar */}
      {isOptionOpen && (
        <TouchableWithoutFeedback onPress={() => setIsOptionOpen(false)}>
          <View
            style={{
              position: 'absolute',
              width: Dimensions.get('window').width - 18,
              height: '100%',
              zIndex: 2,
            }}>
            <View
              style={{
                backgroundColor: warna.putihAsli,
                marginRight: 20,
                alignSelf: 'flex-end',
                elevation: 5,
                paddingHorizontal: 17,
                paddingVertical: 3,
                borderRadius: 2,
              }}>
              {candelete === 1 ? (
                <OptionListPostingan type="delete" onPress={onPressDelete} />
              ) : (
                <OptionListPostingan type="report" onPress={onPressReport} />
              )}
            </View>
          </View>
        </TouchableWithoutFeedback>
      )}

      {/* Nama dan komentar */}
      <Text
        style={{
          color: warna.hitamText2,
          fontSize: 15,
          fontFamily: 'Roboto-Bold',
          marginRight: 11,
        }}>
        {name}
        <Text
          style={{
            fontFamily: 'Roboto-Regular',
            fontWeight: 'normal',
            lineHeight: 18,
          }}>
          {' ' + info}
        </Text>
      </Text>

      {/* Option Komentar */}
      <TouchableOpacity
        onPress={() => setIsOptionOpen(true)}
        style={{
          position: 'absolute',
          right: 0,
          top: 3,
          paddingBottom: 10,
          paddingLeft: 12,
        }}>
        <Icon name="more-v-a" size={17} color={warna.hitamText2} />
      </TouchableOpacity>

      {/* Waktu Posting komentar */}
      <Text
        style={{
          color: 'grey',
          fontSize: 11,
          fontFamily: 'Roboto-Regular',
        }}>
        {moment(createdAt).toNow(true)}
      </Text>
    </View>
  );
};
