import React, {useState, useEffect, useRef} from 'react';

// Component
import {View, TouchableOpacity, Text, TextInput, Alert} from 'react-native';
import MainContainer from 'gsja/src/components/container/ScrollviewContainer';
import {LikeComment} from '../../list/Component';
import {ListKomentar} from './Item';
import {Image} from 'gsja/src/components/common';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import Icon from 'react-native-vector-icons/Fontisto';
import rowStyles from 'gsja/src/assets/styles';
import warna from 'gsja/src/assets/colors';

const {flexStartRow} = rowStyles;

// Function
import {apiPost} from 'gsja/src/services/api';
import {StackActions, NavigationActions} from 'react-navigation';

const DetailPostingan = ({navigation: {getParam}}) => {
  const [isLoading, setLoading] = useState(true);
  const [fetchedData, setFetchedData] = useState([]);
  const [fetchedComments, setFetchedComments] = useState([]);

  // Text Input dan buttonKomentar
  const [textComment, settextComment] = useState('');
  const [replyIdCommunity, setReplyIdCommunity] = useState(null);
  const focusInput = getParam('focusInput');
  const inputCommentRef = useRef(null);

  // getParam dari navigate yg membawa ke halaman ini
  const id_komunitas = getParam('id_komunitas');
  const id_notification = getParam('id_notification');

  // Initial fetch
  useEffect(() => {
    setLoading(true);

    let postData = [
      {
        url: '/getDetailKomunitas',
        data: {
          id_komunitas,
          id_notification,
        },
      },
    ];

    apiPost(postData)
      .then(resp => {
        console.log(resp);
        setFetchedData(resp[0]);
        setFetchedComments(resp[0].comment);
      })
      .then(() => setLoading(false));
  }, [id_notification, id_komunitas]);
  // console.log(id_notification, id_komunitas);
  // Refresh page
  const refreshPage = async () => {
    setLoading(true);

    let postData = [
      {
        url: '/getDetailKomunitas',
        data: {
          id_komunitas,
        },
      },
    ];

    apiPost(postData)
      .then(resp => {
        setFetchedData(resp[0]);
        setFetchedComments(resp[0].comment);
        setReplyIdCommunity(null);
      })
      .then(() => setLoading(false));
  };

  // Fungsi like dan UN-like postingan
  const toggleLikePosting = url => {
    setLoading(true);

    let postData = [
      {
        url,
        data: {
          id_komunitas,
        },
      },
    ];

    apiPost(postData).then(refreshPage);
  };

  // Fungsi submit komentar pada postingan
  const commentPosting = id_komunitasPost => {
    if (!textComment) {
      setReplyIdCommunity(null);
      return null;
    }

    let postData = [
      {
        url: '/commentPost',
        data: {
          id_komunitas: id_komunitasPost ? id_komunitasPost : id_komunitas,
          info: textComment,
        },
      },
    ];

    apiPost(postData).then(() => {
      refreshPage();
      settextComment('');
    });
  };

  // Delete post
  const deleteComment = id_komunitasPost => {
    let postData = [
      {
        url: '/deletePost',
        data: {
          id_komunitas: id_komunitasPost,
        },
      },
    ];

    Alert.alert(
      'Anda Yakin Menghapus Komentar?',
      'Komentar yang sudah dihapus tidak bisa dikembalikan',
      [
        {text: 'Tidak'},
        {
          text: 'Yakin',
          onPress: () => apiPost(postData).then(refreshPage),
        },
      ],
      {cancelable: false},
    );
  };

  // Report post
  const reportComment = id_komunitasPost => {
    let postData = [
      {
        url: '/reportPost',
        data: {
          id_komunitas: id_komunitasPost,
        },
      },
    ];

    Alert.alert(
      'Anda ingin melaporkan komentar ini?',
      'Komentar akan dilaporkan pada Admin',
      [
        {text: 'Tidak'},
        {
          text: 'Yakin',
          onPress: () =>
            apiPost(postData).then(() => {
              refreshPage();
              alert('Komentar telah dilaporkan');
            }),
        },
      ],
      {cancelable: false},
    );
  };

  return (
    <>
      <MainContainer
        isLoading={isLoading}
        children={
          <>
            {/* Bagian atas / judul event  */}
            <View style={flexStartRow}>
              {/* Foto Profil Pembuat Postingan */}
              <View
                style={{
                  height: 45,
                  width: 45,
                  borderRadius: 45,
                  overflow: 'hidden',
                }}>
                <Image
                  source={{uri: fetchedData.fotoprofile}}
                  style={{height: 45, width: 45, borderRadius: 45}}
                />
              </View>

              <View style={{flex: 1, marginLeft: 12}}>
                {/* Text Judul  */}
                <Text
                  style={{
                    color: warna.hitamText2,
                    fontSize: 18,
                    fontFamily: 'Roboto-Medium',
                  }}>
                  {fetchedData.title}
                </Text>

                {/* Text Pembuat Postingan */}
                <View style={[flexStartRow, {marginTop: 5}]}>
                  <Icon name="person" size={16} />
                  <Text
                    style={{
                      flex: 1,
                      color: warna.hitamText2,
                      fontSize: 14,
                      fontFamily: 'Roboto-Regular',
                      marginLeft: 5,
                    }}>
                    {fetchedData.firstname} - {fetchedData.name}
                  </Text>
                </View>
              </View>
            </View>

            {/* Bagian Informasi Konten */}
            <Text
              style={{
                color: warna.hitamText2,
                fontSize: 15,
                fontFamily: 'Roboto-Regular',
                marginTop: 13,
                lineHeight: 22,
              }}>
              {fetchedData.info}
            </Text>

            {/* Gambar/Image informasi  */}
            {fetchedData.gambar && (
              <Image
                source={{uri: fetchedData.gambar}}
                resizeMode="cover"
                style={{height: 200, width: '100%', marginTop: 15}}
              />
            )}

            {/* Jumlah Like dan Comment */}
            <LikeComment
              isLiked={fetchedData.liked}
              countLike={fetchedData.likes}
              countComment={fetchedData.countcom}
              onPressLike={() =>
                toggleLikePosting(
                  fetchedData.liked ? '/unlikePost' : '/likePost',
                )
              }
              onPressComment={() => {
                inputCommentRef.current.focus();
                settextComment('');
                setReplyIdCommunity(null);
              }}
            />

            {/* List komentar */}
            {fetchedComments.map((item, key) => {
              return (
                <ListKomentar
                  key={key}
                  image={item.pimage}
                  name={item.firstname}
                  info={item.info}
                  createdAt={item.t_inserttime}
                  replyList={item.replay}
                  onPressReply={() => {
                    inputCommentRef.current.focus();
                    settextComment(`@${item.firstname} `);
                    setReplyIdCommunity(item.idcommunitypost);
                  }}
                  candelete={item.candelete}
                  onPressDelete={() => deleteComment(item.idcommunitypost)}
                  onPressReport={() => reportComment(item.idcommunitypost)}
                  deleteReply={deleteComment}
                  reportReply={reportComment}
                />
              );
            })}
          </>
        }
      />

      {/* Daftar komentar */}
      <View
        style={[
          flexStartRow,
          {
            paddingLeft: 11,
            borderTopColor: warna.abuabu1,
            borderTopWidth: 3,
          },
        ]}>
        <TextInput
          autoFocus={focusInput && focusInput}
          ref={inputCommentRef}
          style={{
            marginVertical: 4,
            fontSize: 17,
            color: warna.hitamText,
            fontFamily: 'Roboto-Regular',
            flex: 1,
          }}
          placeholder="Tulis komentar"
          autoCapitalize="none"
          returnKeyType="done"
          value={textComment}
          onChangeText={settextComment}
        />

        <TouchableOpacity
          onPress={() => commentPosting(replyIdCommunity && replyIdCommunity)}
          style={{padding: 7, paddingTop: 13}}>
          <Icon
            name="paper-plane"
            size={30}
            style={{paddingRight: 3, flex: 1, alignSelf: 'center'}}
          />
        </TouchableOpacity>
      </View>
    </>
  );
};

DetailPostingan.navigationOptions = ({navigation: {dispatch}}) => {
  return {
    headerTitle: 'Detail Unggahan Berita',
    headerLeft: (
      <NavHeaderButtons>
        <Item
          title="back"
          color="#fff"
          iconName="arrow-left"
          onPress={() => {
            // kembali ke route stack awal
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: 'ListBeranda',
                }),
              ],
            });

            dispatch(resetAction);
          }}
        />
      </NavHeaderButtons>
    ),
  };
};

export default DetailPostingan;
