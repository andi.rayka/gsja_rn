import moment from 'moment';
import {apiPost} from 'gsja/src/services/api';

// return tampilan tanggal kepada user
export const parseDate = fetchedDate => {
  return {
    tgl: moment(fetchedDate).date(),
    bulan: moment(fetchedDate).format('MMM'),
    tahun: moment(fetchedDate).year(),
  };
};

// const likePosting = (id_user, id_komunitas) => {
//   let postData = [
//     {
//       url: '/likePost',
//       data: {id_komunitas},
//     },
//   ];

//   apiPost(postData).then(resp => setListWilayah(resp[0]));

//   await apiPost
//     .post('/', {
//       id_user,
//     })
//     .then(resp => {
//       const {error} = resp.data;

//       if (error === 0) {
//         callbackRefresh();
//       } else {
//         const {error_message} = resp.data;
//         alert(error_message);
//       }
//     })
//     .catch(error => {
//       console.log(error);
//     });
// };
