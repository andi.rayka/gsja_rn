import React from 'react';

// Component
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native';

// Micro Component
import Icon from 'react-native-vector-icons/Fontisto';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import warna from 'gsja/src/assets/colors';
import rowStyles from 'gsja/src/assets/styles';

const {spaceBetweenRow, flexStartRow} = rowStyles;

// Komponen Like dan komentar
const ButtonLikeComment = ({iconName, text, onPress, isLiked}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        spaceBetweenRow,
        {
          paddingTop: 13,
          paddingBottom: 15,
          flex: 1,
          justifyContent: 'center',
        },
      ]}>
      <Icon
        name={iconName}
        size={17}
        color={isLiked ? warna.merahHeader : warna.hitamAsli}
      />
      <Text
        style={{
          color: warna.hitamText2,
          fontSize: 15,
          fontFamily: 'Roboto-Bold',
          marginLeft: 5,
        }}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

export const LikeComment = ({
  isLiked,
  countLike,
  countComment,
  onPressLike,
  onPressComment,
}) => {
  return (
    <View style={[spaceBetweenRow, localStyles.containerLikeComment]}>
      <ButtonLikeComment
        isLiked={isLiked}
        iconName="like"
        text={`${countLike} suka`}
        onPress={onPressLike}
      />

      {/* Separator Like dan Comment */}
      <View
        style={{width: 1.5, height: '70%', backgroundColor: warna.abuabu1}}
      />

      <ButtonLikeComment
        iconName="commenting"
        text={`${countComment} komentar`}
        onPress={onPressComment}
      />
    </View>
  );
};

// Komponen button "baca selanjutnya"
export const ButtonBacaSelanjutnya = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text
        style={{
          color: warna.biruLink,
          fontSize: 15,
          fontFamily: 'Roboto-Regular',
        }}>
        Baca Selanjutnya ...
      </Text>
    </TouchableOpacity>
  );
};

export const OptionListPostingan = ({type, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[flexStartRow, {marginVertical: 9}]}>
      {type === 'delete' ? (
        <Icon name="trash" size={17} color="red" />
      ) : (
        <IconAwesome name="exclamation-triangle" size={17} color="darkorange" />
      )}

      <Text
        style={{
          color: type === 'delete' ? '#bb0000' : '#ff5700',
          fontSize: 15,
          fontFamily: 'Roboto-Medium',
          marginLeft: 10,
        }}>
        {type === 'delete' ? 'Hapus' : 'Report'}
      </Text>
    </TouchableOpacity>
  );
};

export const FloatingButton = ({onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: 55,
        height: 55,
        backgroundColor: warna.merahHeader,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 33,
        right: 27,
        elevation: 10,
      }}>
      <Icon name="plus-a" size={25} color={warna.putihAsli} />
    </TouchableOpacity>
  );
};

const localStyles = StyleSheet.create({
  containerLikeComment: {
    marginTop: 16,
    marginBottom: 15,
    borderTopColor: warna.abuabu1,
    borderBottomColor: warna.abuabu1,
    borderTopWidth: 1.75,
    borderBottomWidth: 1.75,
  },
  textPembuatPost: {
    flex: 1,
    color: warna.hitamText2,
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
    marginLeft: 5,
  },
  textInfoContent: {
    color: warna.hitamText2,
    fontSize: 15,
    fontFamily: 'Roboto-Regular',
    marginTop: 13,
    lineHeight: 22,
  },
});
