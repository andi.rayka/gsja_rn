import React, {useState} from 'react';

// Component
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TouchableWithoutFeedback,
  Dimensions,
  Alert,
} from 'react-native';
import {
  LikeComment,
  ButtonBacaSelanjutnya,
  OptionListPostingan,
} from './Component';
import {Image} from 'gsja/src/components/common';

// Micro Component
import Icon from 'react-native-vector-icons/Fontisto';
import rowStyles from 'gsja/src/assets/styles';
import warna from 'gsja/src/assets/colors';

const {spaceBetweenRow, flexStartRow} = rowStyles;

// Function
import {withNavigation} from 'react-navigation';
import {parseDate} from './Function';
import {StackActions, NavigationActions} from 'react-navigation';
import {apiPost} from 'gsja/src/services/api';

const ListEventItem = ({
  item,
  navigation: {navigate, dispatch},
  filterData,
}) => {
  const [isOpenMenu, setIsOpenMenu] = useState(false);

  const {textDateStart} = styles;

  // Fungsi like dan UN-like postingan
  const toggleLikePosting = (url, id_komunitas) => {
    let postData = [
      {
        url,
        data: {
          id_komunitas,
        },
      },
    ];

    apiPost(postData).then(() => {
      // Jika berhasil, kembali ke route stack awal
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'ListBeranda',
            params: {filterData},
          }),
        ],
      });

      dispatch(resetAction);
    });
  };

  // Delete post
  const deletePost = id_komunitasPost => {
    let postData = [
      {
        url: '/deletePost',
        data: {
          id_komunitas: id_komunitasPost,
        },
      },
    ];

    Alert.alert(
      'Anda Yakin Menghapus Post?',
      'Post yang sudah dihapus tidak bisa dikembalikan',
      [
        {text: 'Tidak'},
        {
          text: 'Yakin',
          onPress: () =>
            apiPost(postData).then(() => {
              // Jika berhasil, kembali ke route stack awal
              const resetAction = StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: 'ListBeranda',
                    params: {filterData},
                  }),
                ],
              });

              dispatch(resetAction);
            }),
        },
      ],
      {cancelable: false},
    );
  };

  // Report post
  const reportComment = id_komunitasPost => {
    let postData = [
      {
        url: '/reportPost',
        data: {
          id_komunitas: id_komunitasPost,
        },
      },
    ];

    Alert.alert(
      'Anda ingin melaporkan Post ini?',
      'Post akan dilaporkan pada Admin',
      [
        {text: 'Tidak'},
        {
          text: 'Yakin',
          onPress: () =>
            apiPost(postData).then(() => {
              setIsOpenMenu(false);
              alert('Post telah dilaporkan');
            }),
        },
      ],
      {cancelable: false},
    );
  };

  // Jika adalah postingan, maka menampilkan komponen untuk Postingan
  if (item.type === 'P') {
    return (
      // Container Utama
      <View
        activeOpacity={0.6}
        style={{
          backgroundColor: warna.putihAsli,
          paddingHorizontal: 11,
          paddingTop: 14,
        }}>
        {/* Pilihan menu di tiap list postingan */}
        {isOpenMenu && (
          <TouchableWithoutFeedback onPress={() => setIsOpenMenu(false)}>
            <View
              style={{
                position: 'absolute',
                width: Dimensions.get('window').width - 18,
                height: '100%',
                zIndex: 2,
                paddingTop: 45,
              }}>
              <View
                style={{
                  backgroundColor: warna.putihAsli,
                  marginRight: 20,
                  alignSelf: 'flex-end',
                  elevation: 5,
                  paddingHorizontal: 17,
                  paddingVertical: 3,
                  borderRadius: 2,
                }}>
                {item.candelete === 1 && (
                  <OptionListPostingan
                    type="delete"
                    onPress={() => deletePost(item.idv)}
                  />
                )}

                {item.candelete === 0 && (
                  <OptionListPostingan
                    type="report"
                    onPress={() => reportComment(item.idv)}
                  />
                )}
              </View>
            </View>
          </TouchableWithoutFeedback>
        )}

        {/* Bagian atas / judul postingan */}
        <View>
          {/* Text Judul */}
          <TouchableOpacity
            onPress={() =>
              navigate('DetailPostingan', {id_komunitas: item.idv})
            }
            style={{marginRight: 50}}>
            <Text
              style={{
                color: warna.hitamText2,
                fontSize: 18,
                fontFamily: 'Roboto-Medium',
              }}>
              {item.name}
            </Text>
          </TouchableOpacity>

          {/* Option menu untuk tiap postingan */}
          <View
            style={[flexStartRow, {position: 'absolute', right: 0, top: 3}]}>
            {/* Jika di pin maka ada icon pin */}
            {item.pinned === '1' && (
              <Icon name="pinboard" size={20} color="#3f51b5" />
            )}
            {/* Button menu/titik tiga */}
            <TouchableOpacity
              onPress={() => setIsOpenMenu(true)}
              style={{
                paddingRight: 6,
                paddingLeft: 19,
                paddingBottom: 3,
                paddingTop: 1,
              }}>
              <Icon name="more-v-a" size={20} />
            </TouchableOpacity>
          </View>

          {/* Text Nama Pembuat post */}
          <View style={[spaceBetweenRow, {marginTop: 5}]}>
            <Icon name="person" size={16} />
            <Text style={styles.textPembuatPost}>
              {`${item.firstname} | ${item.wilayah}`}
            </Text>
          </View>
        </View>

        {/* Bagian Informasi Konten */}
        <Text
          numberOfLines={4}
          ellipsizeMode="tail"
          style={styles.textInfoContent}>
          {item.info}
        </Text>

        <ButtonBacaSelanjutnya
          onPress={() => navigate('DetailPostingan', {id_komunitas: item.idv})}
        />

        {/* Gambar/Image informasi  */}
        {item.image && (
          <Image
            source={{uri: item.image}}
            resizeMode="cover"
            style={{height: 200, width: '100%', marginTop: 14}}
          />
        )}

        {/* Jumlah Like dan Comment */}
        <LikeComment
          isLiked={item.liked}
          countLike={item.likes}
          countComment={item.countcommentar}
          onPressLike={() =>
            toggleLikePosting(
              item.liked ? '/unlikePost' : '/likePost',
              item.idv,
            )
          }
          onPressComment={() =>
            navigate('DetailPostingan', {
              id_komunitas: item.idv,
              focusInput: true,
            })
          }
        />
      </View>
    );
  } else {
    // Jika adalah kegiatan, maka menampilkan komponen untuk kegiatan
    const {datestart, timestart, timeend} = item;

    return (
      // Container Utama
      <View
        activeOpacity={0.6}
        style={{
          backgroundColor: warna.putihAsli,
          paddingHorizontal: 11,
          paddingVertical: 14,
        }}>
        {/* Bagian atas / judul event */}
        <View>
          {/* Text Judul */}
          <TouchableOpacity
            onPress={() => navigate('DetailKegiatan', {id_kegiatan: item.idv})}
            style={{marginRight: 25}}>
            <Text
              style={{
                color: warna.hitamText2,
                fontSize: 18,
                fontFamily: 'Roboto-Medium',
              }}>
              {item.name}
            </Text>
          </TouchableOpacity>

          {/* Jika di pin maka ada icon pin */}
          {item.pinned === '1' && (
            <Icon
              name="pinboard"
              size={20}
              color="#3f51b5"
              style={{position: 'absolute', right: 0, top: 3}}
            />
          )}

          {/* Text Nama BPD pengirim */}
          <View style={[spaceBetweenRow, {marginTop: 5}]}>
            <Icon name="person" size={16} />
            <Text style={styles.textPembuatPost}>{item.wilayah}</Text>
          </View>
        </View>

        {/* Bagian Keterangan Acara  */}
        <TouchableOpacity
          onPress={() => navigate('DetailKegiatan', {id_kegiatan: item.idv})}
          style={[
            spaceBetweenRow,
            {
              paddingTop: 7,
              paddingHorizontal: 9,
              paddingBottom: 4,
              marginTop: 14,
              borderRadius: 5,
              elevation: 2,
            },
          ]}>
          {/* Tanggal acara  */}
          <View
            style={{
              backgroundColor: warna.merahStatusbar,
              borderRadius: 3,
              paddingHorizontal: 15,
              paddingVertical: 5,
            }}>
            <Text style={[textDateStart, {fontSize: 23, lineHeight: 28}]}>
              {parseDate(datestart).tgl}
            </Text>
            <Text style={[textDateStart, {fontSize: 16}]}>
              {parseDate(datestart).bulan}
            </Text>
            <Text style={[textDateStart, {fontSize: 16}]}>
              {parseDate(datestart).tahun}
            </Text>
          </View>

          {/* Tempat Dan waktu acara  */}
          <View
            style={{
              flex: 1,
              marginLeft: 12,
            }}>
            <View style={spaceBetweenRow}>
              <Icon name="map-marker-alt" size={17} />
              <Text style={styles.textPlaceTime}>{item.place}</Text>
            </View>
            <View style={spaceBetweenRow}>
              <Icon name="clock" size={17} />
              <Text style={styles.textPlaceTime}>
                {`${timestart.substr(0, 5)} - ${timeend ? timeend : 'selesai'}`}
              </Text>
            </View>
            <View style={spaceBetweenRow}>
              <Icon name="hourglass-half" size={17} />
              <Text style={styles.textPlaceTime}>
                {`Durasi ${item.days} hari`}
              </Text>
            </View>
          </View>
        </TouchableOpacity>

        {/* Bagian Informasi Konten */}
        <Text
          numberOfLines={4}
          ellipsizeMode="tail"
          style={styles.textInfoContent}>
          {item.info}
        </Text>

        <ButtonBacaSelanjutnya
          onPress={() => navigate('DetailKegiatan', {id_kegiatan: item.idv})}
        />

        {/* Gambar/Image informasi */}
        {item.image && (
          <Image
            source={{uri: item.image}}
            resizeMode="cover"
            style={{height: 200, width: '100%', marginTop: 14}}
          />
        )}
      </View>
    );
  }
};

// Separator/Pembatas antar item
export const itemSeparator = () => (
  <View style={{backgroundColor: warna.abuabu1, height: 11, width: '100%'}} />
);

const styles = StyleSheet.create({
  textDateStart: {
    color: warna.putihAsli,
    textAlign: 'center',
    fontFamily: 'Poppins-Medium',
    lineHeight: 22,
  },
  textPlaceTime: {
    flex: 1,
    color: warna.hitamText,
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    marginLeft: 9,
    marginVertical: 4,
  },
  textInfoContent: {
    color: warna.hitamText2,
    fontSize: 15,
    fontFamily: 'Roboto-Regular',
    marginTop: 13,
    lineHeight: 22,
  },
  textPembuatPost: {
    flex: 1,
    color: warna.hitamText2,
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
    marginLeft: 5,
  },
});

export default withNavigation(ListEventItem);
