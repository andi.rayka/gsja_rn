import React, {useState, useEffect, useRef} from 'react';

// Component
import MainContainer from 'gsja/src/components/container/FlatlistContainer';
import ListItem, {itemSeparator} from './Item';
import ModalFilter, {
  ButtonList,
  ButtonDate,
} from 'gsja/src/components/filterData/ModalFilter';
import ModalSearch from 'gsja/src/components/filterData/ModalSearch';
import {FloatingButton} from './Component';

// Micro Component
import {
  NavHeaderButtons,
  Item,
  NotifIcon,
} from 'gsja/src/components/navigation/HeaderButton';
import strings from 'gsja/src/assets/strings';

const {drawer} = strings;

// Functions
import {apiPost} from 'gsja/src/services/api';

const ListBeranda = ({navigation: {setParams, state, navigate, getParam}}) => {
  // Input Filter
  const [postFilter, setPostFilter] = useState(
    getParam('filterData')
      ? getParam('filterData')
      : {
          keyword: '',
          wilayah: {id: null, name: null},
          tanggal: null,
        },
  );

  // List Pilihan Filter
  const [listWilayah, setListWilayah] = useState([]);

  // Modal
  const [isModalSearchOpen, toggleOpenModalSearch] = useState(false);
  const [isModalFilterOpen, toggleOpenModalFilter] = useState(false);

  // Menghubungkan button di header dengan function di dlm komponen
  const {params = {}} = state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);
  // Bind button with function
  useEffect(() => {
    let postData = [
      {
        url: '/getNotification',
        data: {},
      },
    ];

    apiPost(postData).then(resp => {
      if (resp[0].jmldata != 0) {
        setParam.current({adaNotif: true});
      }

      setParam.current({
        openModalSearch: () => toggleOpenModalSearch(true),
        openModalFilter: () => toggleOpenModalFilter(true),
      });
    });
  }, [paramDesc, setParam]);

  // initial fetch list filter
  useEffect(() => {
    let postData = [
      {
        url: '/getListWilayah',
        data: {},
      },
    ];

    apiPost(postData).then(resp => setListWilayah(resp[0]));
  }, []);

  return (
    <MainContainer
      url="/getListEvent"
      additionalPostData={{...postFilter, wilayah: postFilter.wilayah.id}}
      renderItem={({item}) => <ListItem item={item} filterData={postFilter} />}
      separatorItem={itemSeparator}
      loadingScreenException={
        <>
          <FloatingButton onPress={() => navigate('AddPostingan')} />

          <ModalSearch
            isModalOpen={isModalSearchOpen}
            onClose={() => toggleOpenModalSearch(false)}
            placeholder="kegiatan dan berita"
            onChangeText={text => setPostFilter({...postFilter, keyword: text})}
          />

          <ModalFilter
            isModalOpen={isModalFilterOpen}
            onClose={() => toggleOpenModalFilter(false)}
            onResetFilter={() =>
              setPostFilter({
                ...postFilter,
                wilayah: {id: null, name: null},
                tanggal: null,
              })
            }
            title="Filter"
            child={
              <>
                <ButtonList
                  defaultValueisIndex0
                  title="Wilayah"
                  selectedList={postFilter.wilayah}
                  listOption={listWilayah}
                  saveChange={obj =>
                    setPostFilter({...postFilter, wilayah: obj})
                  }
                  idName="idorganization"
                />

                <ButtonDate
                  title="Tanggal"
                  selectedDate={postFilter.tanggal}
                  saveChange={date =>
                    setPostFilter({...postFilter, tanggal: date})
                  }
                />
              </>
            }
          />
        </>
      }
    />
  );
};

ListBeranda.navigationOptions = ({navigation}) => {
  const {toggleDrawer, navigate, getParam} = navigation;

  return {
    headerTitle: drawer.label.beranda,
    headerLeft: (
      <NavHeaderButtons>
        <Item
          title="drawer"
          color="#fff"
          iconName="nav-icon"
          onPress={toggleDrawer}
        />
      </NavHeaderButtons>
    ),
    headerRight: (
      <NavHeaderButtons>
        <Item
          title="notif"
          color={getParam('adaNotif') ? '#fce205' : '#fff'}
          iconName="bell-alt"
          onPress={() => navigate('Notifikasi')}
        />
        <Item
          title="filter"
          color="#fff"
          iconName="filter"
          onPress={getParam('openModalFilter')}
        />
        <Item
          title="search"
          color="#fff"
          iconName="search"
          onPress={getParam('openModalSearch')}
        />
      </NavHeaderButtons>
    ),
  };
};

export default ListBeranda;
