import React, {useState, useEffect, useRef} from 'react';

// Component
import {
  View,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
} from 'react-native';
import MainContainer from 'gsja/src/components/container/ScrollviewContainer';
import {Image} from 'gsja/src/components/common';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import Icon from 'react-native-vector-icons/Fontisto';
import rowStyles from 'gsja/src/assets/styles';
import warna from 'gsja/src/assets/colors';

const {flexStartRow, spaceBetweenRow} = rowStyles;

// Function
import {apiPost} from 'gsja/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';
import {StackActions, NavigationActions} from 'react-navigation';

const AddPostingan = ({navigation: {state, setParams, dispatch}}) => {
  const [isLoading, setLoading] = useState(false);
  const [userData, setUserData] = useState({});
  const [selectedImage, setSelectedImage] = useState(null);

  // Text Input
  const [textIsi, setTextIsi] = useState('');
  const [textJudul, setTextJudul] = useState('');
  const focusInput = useRef(null);

  // Menghubungkan button di header dengan function di dlm komponen
  const {params = {}} = state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);
  // Bind button with function
  useEffect(() => {
    const addPost = () => {
      // Jika salah satu judul atau isi kosong
      if (!textJudul || !textIsi) {
        alert('Masukkan judul dan isi post');

        return null;
      }

      setLoading(true);

      let postData = [
        {
          url: '/postKomunitas',
          data: {
            judul: textJudul,
            isi: textIsi,
            gambar: selectedImage ? selectedImage.base64Image : null,
          },
        },
      ];

      apiPost(postData)
        .then(resp => {
          // Jika edit profil berhasil, kembali ke route stack awal, yaitu detail profil
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'ListBeranda'})],
          });

          dispatch(resetAction);
        })
        .then(() => setLoading(false));
    };

    setParam.current({addPost});
  }, [paramDesc, setParam, textJudul, textIsi, selectedImage, dispatch]);

  // Ambil User Data dari Async Storage
  useEffect(() => {
    const getUserData = async () => {
      try {
        const data = await AsyncStorage.getItem('userData');

        setUserData(JSON.parse(data));
      } catch (error) {
        console.log(error.message);
      }
    };

    getUserData();
  }, []);

  // Ganti Foto Profil
  const getImage = type => {
    let methodName;
    if (type === 'camera') {
      methodName = 'launchCamera';
    } else {
      methodName = 'launchImageLibrary';
    }

    ImagePicker[methodName]({mediaType: 'photo'}, resp => {
      const {uri, data, didCancel, error} = resp;
      console.log('Response = ', resp);
      if (didCancel || error) {
        return null;
      }
      setSelectedImage({image: uri, base64Image: data});
    });
  };

  return (
    <>
      <MainContainer
        isLoading={isLoading}
        children={
          <>
            {/* Foto profil dan nama user */}
            <View
              style={[
                flexStartRow,
                {
                  borderBottomColor: warna.hitamText2,
                  borderBottomWidth: 1,
                  paddingBottom: 10,
                },
              ]}>
              <View
                style={{
                  width: 40,
                  height: 40,
                  borderRadius: 40,
                  overflow: 'hidden',
                }}>
                <Image
                  source={{uri: userData.foto}}
                  style={{width: 40, height: 40, borderRadius: 40}}
                />
              </View>

              {/* Input Text JUDUL */}
              <TextInput
                autoFocus
                autoCapitalize="words"
                value={textJudul}
                onChangeText={setTextJudul}
                placeholder="Tulis judul di sini..."
                style={{
                  fontSize: 16,
                  fontFamily: 'Roboto-Medium',
                  color: warna.hitamText2,
                  marginLeft: 7,
                  flex: 1,
                }}
                returnKeyType="next"
                onEndEditing={() => focusInput.current.focus()}
              />
            </View>

            {/* Input Text ISI */}
            <TextInput
              ref={focusInput}
              multiline
              value={textIsi}
              onChangeText={setTextIsi}
              placeholder="Tulis post di sini..."
              style={{
                fontSize: 16,
                fontFamily: 'Roboto-Medium',
                color: warna.hitamText2,
              }}
            />

            <TouchableWithoutFeedback
              onPress={() => focusInput.current.focus()}>
              <View style={{flex: 1}} />
            </TouchableWithoutFeedback>

            {selectedImage && (
              <Image
                source={{uri: selectedImage.image}}
                style={{height: 200, width: '100%', marginTop: 14}}
              />
            )}
          </>
        }
      />

      {/* Button ambil foto dari kamera dan galeri */}
      <View style={[spaceBetweenRow, {elevation: 10}]}>
        <ButtonAddImage type="camera" onPress={() => getImage('camera')} />

        <View
          style={{height: 36, width: 1, backgroundColor: 'rgba(0, 0, 0, 0.39)'}}
        />

        <ButtonAddImage type="gallery" onPress={() => getImage('gallery')} />
      </View>
    </>
  );
};

const ButtonAddImage = ({type, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        paddingVertical: 16,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Icon name={type === 'camera' ? 'camera' : 'picture'} size={30} />
    </TouchableOpacity>
  );
};

AddPostingan.navigationOptions = ({navigation: {goBack, getParam}}) => ({
  headerTitle: 'Unggah Berita Baru',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#fff"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="addPost"
        color="#fff"
        iconName="paper-plane"
        onPress={getParam('addPost')}
      />
    </NavHeaderButtons>
  ),
});

export default AddPostingan;
