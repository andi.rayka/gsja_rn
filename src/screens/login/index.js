import React, {useState, useRef, useEffect} from 'react';

// Component halo?
import {
  View,
  StatusBar,
  ImageBackground,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Modal,
} from 'react-native';
import {Image} from 'gsja/src/components/common';

// Micro Component
import Icon from 'react-native-vector-icons/Fontisto';
import LoadingScreen from 'gsja/src/components/common/LoadingScreen';

// Function
import {apiAuth} from 'gsja/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';
import {setAsyncStorage} from 'gsja/src/commonFunctions';

const Login = ({navigation: {navigate}}) => {
  const [isLoading, setLoading] = useState(false);

  // const [username, setUsername] = useState('9011311');
  // const [password, setPassword] = useState('000000');

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const refPassword = useRef(null);

  useEffect(() => {
    AsyncStorage.clear();
  }, []);

  // Handle Submit
  const submitLogin = () => {
    setLoading(true);

    let postData = {
      username,
      password,
      device: 'A.1.D',
    };

    apiAuth('/login', postData, false)
      .then(resp => {
        // console.log('Login resp: ', resp);

        // Simpan User Data
        const {token, iduser, idrole} = resp;
        setAsyncStorage('loginToken', token);
        setAsyncStorage('idUser', iduser);

        // Navigate berdasarkan response
        // Jika punya lebih dari 1 role, ke halaman pilihAkses
        if (resp.num_role > 1) {
          setLoading(false);
          navigate('PilihAkses');
          // Jika cuma punya 1, maka langsung ke beranda
        } else {
          navigate('drawerBeranda');

          // Jika idrole salah satu dari dua ini, maka dia adalah
          // admin BPP atau BPD yang mana memiliki hak tertentu
          if (
            idrole === 'e5306596-6f29-4368-9879-063b455457f8' ||
            idrole === 'e4179f0d-f0d6-4a30-8fa0-305ec246736e'
          ) {
            setAsyncStorage('isAdmin', '1');
          }

          // Simpan userData
          setAsyncStorage('userData', JSON.stringify(resp));
        }
      })
      .catch(() => setLoading(false));
  };

  return (
    // Container Screen
    <ImageBackground
      source={{uri: 'bg_login'}}
      style={{
        flex: 1,
        paddingHorizontal: 40,
        paddingTop: 5,
        justifyContent: 'center',
      }}>
      <StatusBar translucent backgroundColor="transparent" />

      {/* logo  */}
      <Image
        source={{uri: 'ic_gsja'}}
        style={{
          width: 115,
          height: 115,
          alignSelf: 'center',
        }}
      />

      {/* Judul */}
      <Text
        style={{
          color: '#fff',
          fontSize: 35,
          fontFamily: 'Roboto-Bold',
          marginTop: 20,
          textAlign: 'center',
        }}>
        GSJA SMART
      </Text>

      <Text
        style={{
          color: '#fff',
          fontSize: 22,
          fontFamily: 'Roboto-Regular',
          marginTop: 50,
          marginBottom: 25,
          textAlign: 'center',
        }}>
        LOGIN
      </Text>

      {/* Form Username  */}
      <View>
        <Icon name="person" style={styles.iconForm} />
        <TextInput
          placeholder="username"
          placeholderTextColor="#fff"
          autoCapitalize="none"
          returnKeyType="next"
          value={username}
          onChangeText={setUsername}
          style={styles.formLogin}
          onEndEditing={() => refPassword.current.focus()}
        />
      </View>

      {/* Form Password  */}
      <View style={styles.containerForm}>
        <Icon name="locked" style={styles.iconForm} />
        <TextInput
          placeholder="password"
          placeholderTextColor="#fff"
          autoCapitalize="none"
          returnKeyType="done"
          value={password}
          onChangeText={setPassword}
          style={styles.formLogin}
          secureTextEntry
          ref={refPassword}
        />
      </View>

      {/* Button Submit */}
      <TouchableOpacity style={styles.buttonSubmit} onPress={submitLogin}>
        <Text style={styles.textSubmit}>Login</Text>
      </TouchableOpacity>

      {/* Modal yang muncul sbg loading screen */}
      <Modal
        animationType="fade"
        transparent
        visible={isLoading}
        onRequestClose={() => setLoading(false)}>
        <View style={{flex: 1, backgroundColor: '#000', opacity: 0.5}}>
          <LoadingScreen />
        </View>
      </Modal>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  containerForm: {
    marginTop: 20,
  },
  formLogin: {
    backgroundColor: 'rgba(178, 147, 147, 0.45)',
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 30,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 45,
    fontSize: 17,
    lineHeight: 20,
    color: '#fff',
    fontFamily: 'Roboto-Regular',
  },
  iconForm: {
    fontSize: 22,
    color: '#fff',
    position: 'absolute',
    top: 10,
    left: 15,
    zIndex: 2,
  },
  buttonSubmit: {
    height: 50,
    marginTop: 35,
    backgroundColor: '#b70000',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSubmit: {
    fontFamily: 'Roboto-Medium',
    color: '#fff',
    fontSize: 17,
    fontWeight: '600',
  },
});

export default Login;
