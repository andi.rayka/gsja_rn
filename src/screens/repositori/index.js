import React, {useState, useEffect, useRef} from 'react';

// Component
import {
  StatusBar,
  Text,
  TouchableOpacity,
  ScrollView,
  PermissionsAndroid,
  View,
  Dimensions,
} from 'react-native';
import ModalSearch from 'gsja/src/components/filterData/ModalSearch';
import {Image} from 'gsja/src/components/common';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import strings from 'gsja/src/assets/strings';
import warna from 'gsja/src/assets/colors';
import rowStyles from 'gsja/src/assets/styles';
import Icon from 'react-native-vector-icons/Fontisto';

const {drawer} = strings;
const {flexStartRow} = rowStyles;

// Function
import {apiPost} from 'gsja/src/services/api';
import RNFetchBlob from 'rn-fetch-blob';

const Arsip = ({navigation: {state, setParams}}) => {
  const [isLoading, setLoading] = useState(true);

  // List
  const [folderList, setFolderList] = useState([]);
  const [fileList, setFileList] = useState([]);

  // Search
  const [keyword, setKeyword] = useState('');

  // Modal
  const [isModalSearchOpen, toggleOpenModalSearch] = useState(false);

  // Menghubungkan button di header dengan function di dlm komponen
  const {params = {}} = state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);
  // Bind button with function
  useEffect(() => {
    setParam.current({openModalSearch: () => toggleOpenModalSearch(true)});
  }, [paramDesc, setParam]);

  // fetch repositori
  useEffect(() => {
    setLoading(true);

    let postData = [
      {
        url: '/getListRepositori',
        data: {
          nama: keyword,
        },
      },
    ];

    apiPost(postData)
      .then(resp => {
        const {folder, allfiles} = resp[0];

        setFolderList(folder);
        setFileList(allfiles);
      })
      .then(() => setLoading(false));
  }, [keyword]);

  // ketika ke folder ketika diklik
  const masukFolder = idrepositori => {
    setLoading(true);
    let postData = [
      {
        url: '/getListRepositori',
        data: {
          idrepositori,
        },
      },
    ];

    apiPost(postData)
      .then(resp => {
        const {folder, allfiles} = resp[0];

        setFolderList(folder);
        setFileList(allfiles);
        setKeyword('');
      })
      .then(() => setLoading(false));
  };

  // Download Lampiran
  // -> base64 String yang mau disimpan sebagai file
  const downloadFile = async (filename, base64Content) => {
    try {
      // izin ke pengguna
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Izin menyimpan file',
          message: 'Aplikasi meminta izin mengakses penyimpanan',
          buttonNeutral: 'Tanya lagi nanti',
          buttonNegative: 'Tolak',
          buttonPositive: 'Izinkan',
        },
      );
      // Jika diizinkan
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        const {mkdir, isDir, writeFile} = RNFetchBlob.fs;
        const path = '/storage/emulated/0/Arsip GSJA';

        setLoading(true);

        // Jika folder belum ada maka buat folder Arsip GSJA
        if (isDir(path) === false) {
          mkdir(path).catch(err => console.log(err));
        }

        writeFile(`${path}/${filename}`, base64Content, 'base64')
          .then(res => {
            alert('File berhasil diunduh di folder Arsip GSJA');
          })
          .catch(error => {
            alert('Gagal menyimpan data ke penyimpanan');
            console.log(error);
          });

        // Jika tidak diizinkan
      } else {
        alert('Tidak mendapat izin sehingga tidak bisa menyimpan file');
      }

      setLoading(false);
    } catch (err) {
      console.log(err);
    }
  };

  if (isLoading) {
    return (
      <ScrollView
        contentContainerStyle={{backgroundColor: warna.abuabu1, flexGrow: 1}}>
        <StatusBar backgroundColor={warna.merahStatusbar} />

        {/* Tombol kembali ke folder sebelumnya */}
        <ButtonKeluarFolder disabled />

        <View
          style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap', padding: 4}}>
          <Text
            style={{
              color: warna.hitamText,
              fontSize: 25,
              fontFamily: 'Roboto-Medium',
              marginTop: 15,
              marginHorizontal: 10,
              textAlign: 'center',
              flex: 1,
            }}>
            Loading...
          </Text>
        </View>

        {/* Modal Search */}
        <ModalSearch
          isModalOpen={isModalSearchOpen}
          onClose={() => toggleOpenModalSearch(false)}
          placeholder="nama repositori"
          onChangeText={setKeyword}
        />
      </ScrollView>
    );
  }

  return (
    <ScrollView
      contentContainerStyle={{backgroundColor: warna.abuabu1, flexGrow: 1}}>
      <StatusBar backgroundColor={warna.merahStatusbar} />

      <ButtonKeluarFolder
        onPress={() => masukFolder(folderList.idparentinfo)}
      />

      <View
        style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap', padding: 4}}>
        {/* Folder  */}
        {folderList.map((item, key) => {
          return (
            <DirectoryComponent
              key={key}
              type="folder"
              text={item.name}
              onPress={() => masukFolder(item.idinfo)}
            />
          );
        })}

        {/* File  */}
        {fileList.map((item, key) => {
          const {name, orgname, content} = item;
          let type;

          switch (item.tipefile) {
            case 'pdf':
              type = 'pdf';
              break;
            case 'doc':
            case 'docx':
              type = 'word';
              break;
            case 'xls':
            case 'xlsx':
              type = 'excel';
              break;
            case 'ppt':
            case 'pptx':
              type = 'ppt';
              break;

            default:
              type = 'file';
              break;
          }

          return (
            <DirectoryComponent
              key={key}
              type={type}
              text={name}
              onPress={() => downloadFile(orgname, content)}
            />
          );
        })}

        {/* Text jika tidak ada isi dalam folder */}
        {folderList.length < 1 && fileList.length < 1 && (
          <Text
            style={{
              color: warna.hitamText,
              fontSize: 25,
              fontFamily: 'Roboto-Medium',
              marginTop: 15,
              marginHorizontal: 10,
              textAlign: 'center',
              flex: 1,
            }}>
            Tidak Ada Dokumen
          </Text>
        )}
      </View>

      {/* Modal Search */}
      <ModalSearch
        isModalOpen={isModalSearchOpen}
        onClose={() => toggleOpenModalSearch(false)}
        placeholder="nama repositori"
        onChangeText={setKeyword}
      />
    </ScrollView>
  );
};

// Komponen Untuk menampilkan ikon dan nama folder/file
// Type yang bisa dipakai adalah: folder, word, pdf, excel
const DirectoryComponent = ({type, text, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        backgroundColor: warna.putihAsli,
        width: 120,
        height: 120,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
      }}>
      <Image source={{uri: `ic_${type}`}} style={{width: 75, height: 75}} />
      <Text
        numberOfLines={1}
        ellipsizeMode="tail"
        style={{
          color: warna.hitamText,
          fontSize: 13,
          fontFamily: 'Roboto-Regular',
          marginTop: 5,
          marginHorizontal: 10,
        }}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

// Tombol kembali ke folder sebelumnya
const ButtonKeluarFolder = ({onPress, disabled}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={[
        flexStartRow,
        {
          marginBottom: 2,
          paddingVertical: 10,
          backgroundColor: '#c4c4c4',
          width: Dimensions.get('window').width,
        },
      ]}>
      <Icon name="arrow-left" size={23} color={warna.hitamText} />

      <Text
        style={{
          color: warna.hitamText,
          fontSize: 17,
          fontFamily: 'Roboto-Regular',
          marginLeft: 10,
        }}>
        Kembali ke folder sebelumnya
      </Text>
    </TouchableOpacity>
  );
};

Arsip.navigationOptions = ({navigation: {toggleDrawer, getParam}}) => ({
  headerTitle: drawer.label.repositori,
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="drawer"
        color="#fff"
        iconName="nav-icon"
        onPress={toggleDrawer}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="search"
        color="#fff"
        iconName="search"
        onPress={getParam('openModalSearch')}
      />
    </NavHeaderButtons>
  ),
});

export default Arsip;
