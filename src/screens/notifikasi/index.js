import React from 'react';

// Component
import MainContainer from 'gsja/src/components/container/FlatlistContainer';
import ListItem, {itemSeparator} from './Item';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';

const ListNotifikasi = () => {
  return (
    <MainContainer
      url="/getNotification"
      renderItem={({item}) => <ListItem item={item} />}
      separatorItem={itemSeparator}
    />
  );
};

ListNotifikasi.navigationOptions = ({navigation: {goBack}}) => ({
  headerTitle: 'Notifikasi',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#fff"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
});

export default ListNotifikasi;
