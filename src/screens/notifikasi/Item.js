import React from 'react';

// Component
import {View, Text, TouchableOpacity} from 'react-native';

// Function
import {withNavigation} from 'react-navigation';

const ListNotifikasiItem = ({item, navigation: {navigate}}) => {
  // ketika salah satu list notif dipencet, akan navigate ke Detail Komunitas atau Kegiatan.
  // Kalau navigate dari notif, bukan dari list-nya, maka ada tambahan id_notifcation
  // di bagian panggil api pada detailnya.
  const onPressNotif = (type, idDetail, id_notification) => {
    if (type === 'P') {
      navigate('DetailPostingan', {
        id_notification,
        id_komunitas: idDetail,
      });
    } else {
      navigate('DetailKegiatan', {
        id_notification,
        id_kegiatan: idDetail,
      });
    }
  };

  const {type, id, idnotification} = item;
  return (
    // Container Utama
    <TouchableOpacity
      onPress={() => onPressNotif(type, id, idnotification)}
      activeOpacity={0.6}
      style={{
        backgroundColor: '#fff',
        paddingHorizontal: 10,
        paddingVertical: 22,
        elevation: 3,
      }}>
      <Text
        style={{
          fontSize: 16,
          color: '#505050',
          fontFamily: 'Roboto-Regular',
        }}>
        {item.notenotif}
      </Text>
    </TouchableOpacity>
  );
};

// Separator/Pembatas antar item
export const itemSeparator = () => (
  <View style={{backgroundColor: '#e9e9e9', height: 0.5, width: '100%'}} />
);

export default withNavigation(ListNotifikasiItem);
