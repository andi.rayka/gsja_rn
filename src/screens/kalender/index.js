import React, {useState, useEffect, useRef} from 'react';

// Component
import {View, Text, ScrollView, StatusBar} from 'react-native';
import {CalendarList, LocaleConfig} from 'react-native-calendars';
import ModalFilter, {
  ButtonList,
} from 'gsja/src/components/filterData/ModalFilter';
import {ItemListAgenda, DataKalenderBhsIndonesia} from './Item';

// Micro Component
import {
  NavHeaderButtons,
  Item,
} from 'gsja/src/components/navigation/HeaderButton';
import strings from 'gsja/src/assets/strings';

const {drawer} = strings;

// Function
import {apiPost} from 'gsja/src/services/api';
import moment from 'moment';

// Ganti kalender menjadi bahasa indonesia
LocaleConfig.locales['id'] = DataKalenderBhsIndonesia;
LocaleConfig.defaultLocale = 'id';

const Kalender = ({navigation: {navigate, state, setParams}}) => {
  const [isLoading, setLoading] = useState(true);
  const [calendarDot, setCalendarDot] = useState({});
  const [listKegiatan, setListKegiatan] = useState([]);
  const [tanggal, setTanggal] = useState(moment().format('YYYY-MM-DD'));

  // Input Filter
  const [postFilter, setPostFilter] = useState({
    wilayah: {id: null, name: null},
  });

  // List Pilihan Filter
  const [listWilayah, setListWilayah] = useState([]);

  // Modal open
  const [isModalFilterOpen, toggleOpenModalFilter] = useState(false);

  // Menghubungkan button di header dengan function di dlm komponen
  const {params = {}} = state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);
  // Bind button with function
  useEffect(() => {
    setParam.current({openModalFilter: () => toggleOpenModalFilter(true)});
  }, [paramDesc, setParam]);

  // Ambil tanggal yang berisi kegiatan.
  // Diambil sekali untuk memberi dot/titik/tanda pada tangal.
  useEffect(() => {
    let postData = [
      {
        url: '/getListWilayah',
        data: {},
      },
    ];

    apiPost(postData).then(resp => {
      // Menambah pilihan filter Majelis Pusat ke urutan dua
      resp[0].splice(1, 0, {
        idorganization: '99',
        name: 'Majelis Pusat',
      });

      setListWilayah(resp[0]);
    });
  }, []);

  // Ambil list kegiatan tiap tanggal
  useEffect(() => {
    setLoading(true);

    let postData = [
      {
        url: '/getCalendar',
        data: {
          tanggal,
          wilayah: postFilter.wilayah.id,
        },
      },
      {
        url: '/getPointCalendar',
        data: {
          wilayah: postFilter.wilayah.id,
        },
      },
    ];

    apiPost(postData)
      .then(resp => {
        setListKegiatan(resp[0]);

        setCalendarDot(resp[1]);
      })
      .then(() => setLoading(false));
  }, [tanggal, postFilter]);

  // List kegiatan
  const showListKegiatan = () => {
    if (isLoading) {
      return <Text style={{fontSize: 16}}>Memuat kegiatan...</Text>;
    }

    if (listKegiatan.length > 0) {
      return listKegiatan.map((item, key) => {
        return (
          <ItemListAgenda
            key={key}
            item={item}
            onPress={() =>
              navigate('DetailKegiatan', {id_kegiatan: item.idevent})
            }
          />
        );
      });
    } else {
      return (
        <Text style={{fontSize: 16}}>Tidak ada kegiatan di tanggal ini</Text>
      );
    }
  };

  return (
    <ScrollView contentContainerStyle={{paddingBottom: 220}}>
      <StatusBar backgroundColor="#5e0101" />

      <CalendarList
        pastScrollRange={0}
        horizontal
        pagingEnabled
        onDayPress={({dateString}) => setTanggal(dateString)}
        theme={{dotColor: 'red'}}
        markedDates={calendarDot}
        style={{borderBottomWidth: 1, borderBottomColor: '#000'}}
      />

      <View style={{paddingHorizontal: 10}}>{showListKegiatan()}</View>

      {/* Modal Filter */}
      <ModalFilter
        isModalOpen={isModalFilterOpen}
        onClose={() => toggleOpenModalFilter(false)}
        onResetFilter={() =>
          setPostFilter({
            ...postFilter,
            wilayah: {id: null, name: null},
          })
        }
        title="Filter Kalender"
        child={
          <ButtonList
            defaultValueisIndex0
            title="Wilayah"
            selectedList={postFilter.wilayah}
            listOption={listWilayah}
            saveChange={obj => setPostFilter({...postFilter, wilayah: obj})}
            idName="idorganization"
          />
        }
      />
    </ScrollView>
  );
};

Kalender.navigationOptions = ({navigation: {toggleDrawer, getParam}}) => ({
  headerTitle: drawer.label.kalender,
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="drawer"
        color="#fff"
        iconName="nav-icon"
        onPress={toggleDrawer}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="filter"
        color="#fff"
        iconName="filter"
        onPress={getParam('openModalFilter')}
      />
    </NavHeaderButtons>
  ),
});

export default Kalender;
