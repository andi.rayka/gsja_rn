import React from 'react';

// Component
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

// Micro Component
import Icon from 'react-native-vector-icons/Fontisto';
import rowStyles from 'gsja/src/assets/styles';

const {spaceBetweenRow} = rowStyles;

// Function
import moment from 'moment';

export const ItemListAgenda = ({item, onPress}) => {
  const {textDateStart} = styles;
  const {datestart} = item;

  // Tampilkan tanggal kepada user
  const parseDate = fetchedDate => {
    return {
      tgl: moment(fetchedDate).date(),
      bulan: moment(fetchedDate).format('MMM'),
      tahun: moment(fetchedDate).year(),
    };
  };

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        spaceBetweenRow,
        {
          paddingTop: 7,
          paddingHorizontal: 9,
          paddingBottom: 4,
          marginTop: 14,
          elevation: 2,

          borderLeftColor: '#ff0707',
          borderLeftWidth: 8,
        },
      ]}>
      {/* Tanggal acara  */}
      <View
        style={{
          paddingHorizontal: 14,
          paddingTop: 8,
          height: '100%',
        }}>
        <Text style={[textDateStart, {fontSize: 27, lineHeight: 35}]}>
          {parseDate(datestart).tgl}
        </Text>
        <Text style={[textDateStart, {fontSize: 18}]}>
          {parseDate(datestart).bulan}
        </Text>
        <Text
          style={[textDateStart, {fontSize: 18, fontFamily: 'Roboto-Light'}]}>
          {parseDate(datestart).tahun}
        </Text>
      </View>

      {/* Nama, Tempat, dan waktu acara  */}
      <View
        style={{
          flex: 1,
          marginLeft: 12,
        }}>
        {/* Nama */}
        <Text
          style={{
            flex: 1,
            color: '#505050',
            fontSize: 17,
            fontFamily: 'Roboto-Medium',
          }}>
          {item.name}
        </Text>
        {/* Place */}
        <View style={spaceBetweenRow}>
          <Icon name="map-marker-alt" size={17} />
          <Text style={styles.textPlaceTime}>{item.place}</Text>
        </View>
        {/* Time start-end */}
        <View style={spaceBetweenRow}>
          <Icon name="clock" size={17} />
          <Text style={styles.textPlaceTime}>
            {`${item.timestart.substr(0, 5)} - ${
              item.timeend ? item.timeend : 'selesai'
            }`}
          </Text>
        </View>
        {/* Durasi acara */}
        <View style={spaceBetweenRow}>
          <Icon name="hourglass-half" size={17} />
          <Text style={styles.textPlaceTime}>{`Durasi ${item.days} hari`}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textDateStart: {
    color: '#ff0707',
    textAlign: 'center',
    fontFamily: 'Roboto-Medium',
    lineHeight: 22,
  },
  textPlaceTime: {
    flex: 1,
    color: '#303030',
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    marginLeft: 9,
    marginVertical: 3.3,
  },
  textJumlahLikeComment: {
    color: '#505050',
    fontSize: 15,
    fontFamily: 'Roboto-Regular',
    marginLeft: 4,
  },
});

export const DataKalenderBhsIndonesia = {
  monthNames: [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember',
  ],
  monthNamesShort: [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'Mei',
    'Jun',
    'Jul',
    'Agus',
    'Sep',
    'Okt',
    'Nov',
    'Des',
  ],
  dayNames: ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'],
  dayNamesShort: ['Sen', 'Sel', 'Rab', 'Kam', 'jum', 'Sab', 'Ming'],
  today: 'Hari ini',
};
