import React, {useEffect, useState} from 'react';

// Component
import {
  Animated,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

// Micro Component
import LoadingScreen from 'gsja/src/components/common/LoadingScreen';

// Function
import {apiPost, apiAuth} from 'gsja/src/services/api';
import {setAsyncStorage} from 'gsja/src/commonFunctions';

const HEADER_MAX_HEIGHT = 300;
const HEADER_MIN_HEIGHT = 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const PilihRole = ({navigation: {navigate}}) => {
  const [isLoading, setLoading] = useState(true);
  const [fetchedData, setFetchedData] = useState([]);

  const statescrollY = new Animated.Value(0);

  // Ambil daftar akses
  // initial fetch
  useEffect(() => {
    let postData = [
      {
        url: '/getAkses',
        data: {},
      },
    ];

    apiPost(postData).then(resp => setFetchedData(resp[0]));
    setLoading(false);
  }, []);

  // Pilih Akses dari list
  const setAkses = async (idmember, idrole, idorganization) => {
    setLoading(true);
    let postData = {
      idmember,
      idrole,
      idorganization,
    };

    apiAuth('/setAkses', postData, true)
      .then(resp => {
        // Jika idrole salah satu dari dua ini, maka dia adalah
        // admin BPP atau BPD yang mana memiliki hak tertentu
        if (
          resp.idrole === 'e5306596-6f29-4368-9879-063b455457f8' ||
          resp.idrole === 'e4179f0d-f0d6-4a30-8fa0-305ec246736e'
        ) {
          setAsyncStorage('isAdmin', '1');
        }

        setAsyncStorage('userData', JSON.stringify(resp));

        navigate('drawerBeranda');
      })
      .catch(() => setLoading(false));
  };

  const _renderScrollViewContent = () => {
    return (
      <View style={{paddingTop: HEADER_MAX_HEIGHT + 20, paddingBottom: 20}}>
        {fetchedData.map((item, key) => {
          const {idmember, idrole, idorganization} = item;

          return (
            <TouchableOpacity
              key={key}
              onPress={() =>
                setAkses(idmember, idrole, idorganization, item.name)
              }
              style={{
                elevation: 3,
                borderRadius: 3,
                marginTop: 5,
                marginBottom: 15,
                marginHorizontal: 15,
                paddingTop: 23,
                paddingBottom: 21,
                paddingHorizontal: 16,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  color: '#303030',
                  fontFamily: 'Roboto-Medium',
                }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  const scrollY = Animated.add(statescrollY, 0);
  const headerTranslate = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -HEADER_SCROLL_DISTANCE],
    extrapolate: 'clamp',
  });
  const imageOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0],
    extrapolate: 'clamp',
  });
  const imageTranslate = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });
  const titleScale = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0.8],
    extrapolate: 'clamp',
  });
  const titleTranslate = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 0, -8],
    extrapolate: 'clamp',
  });

  return (
    <>
      <StatusBar translucent backgroundColor="transparent" />
      <Animated.ScrollView
        scrollEventThrottle={1}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: statescrollY}}}],
          {useNativeDriver: true},
        )}
        // iOS offset for RefreshControl
        contentInset={{
          top: HEADER_MAX_HEIGHT,
        }}
        contentOffset={{
          y: -HEADER_MAX_HEIGHT,
        }}>
        {isLoading ? <LoadingScreen /> : _renderScrollViewContent()}
      </Animated.ScrollView>
      <Animated.View
        pointerEvents="none"
        style={[styles.header, {transform: [{translateY: headerTranslate}]}]}>
        <Animated.Image
          style={[
            styles.backgroundImage,
            {
              opacity: imageOpacity,
              transform: [{translateY: imageTranslate}],
            },
          ]}
          source={{uri: 'bg_login'}}
        />
      </Animated.View>
      <Animated.View
        style={[
          styles.bar,
          {
            transform: [{scale: titleScale}, {translateY: titleTranslate}],
          },
        ]}>
        <Text style={styles.title}>Pilih Akses</Text>
      </Animated.View>
    </>
  );
};

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#38ee7d',
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  bar: {
    marginTop: Platform.OS === 'ios' ? 28 : 38,
    height: 32,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    fontSize: 28,
    fontFamily: 'OpenSans-SemiBold',
    color: '#fff',
    marginLeft: 20,
  },
});

export default PilihRole;
