import {createDrawerNavigator} from 'react-navigation-drawer';
import strings from 'gsja/src/assets/strings';

// Component
import React from 'react';
import {Dimensions} from 'react-native';
import CustomDrawer, {IconDrawer} from 'gsja/src/components/navigation/Drawer';

import {
  stackKartuNama,
  stackEditProfil,
  stackEditGereja,
  stackBeranda,
  stackKalender,
  stackPendeta,
  stackGereja,
  stackInstitusi,
  stackRepositori,
  stackStatistik,
  stackBantuan,
} from './stack';

const {
  drawer: {label},
} = strings;
const drawerNavigator = createDrawerNavigator(
  {
    drawerEditProfil: {
      screen: stackEditProfil,
      navigationOptions: {
        drawerLabel: () => null,
      },
    },
    drawerEditGereja: {
      screen: stackEditGereja,
      navigationOptions: {
        drawerLabel: () => null,
      },
    },
    drawerKartuNama: {
      screen: stackKartuNama,
      navigationOptions: {
        drawerLabel: () => null,
      },
    },
    drawerBeranda: {
      screen: stackBeranda,
      navigationOptions: {
        drawerLabel: label.beranda,
        drawerIcon: <IconDrawer name="ic_drawer_home" />,
      },
    },
    drawerKalender: {
      screen: stackKalender,
      navigationOptions: {
        drawerLabel: label.kalender,
        drawerIcon: <IconDrawer name="ic_drawer_kalender" />,
      },
    },
    drawerBantuan: {
      screen: stackBantuan,
      navigationOptions: {
        drawerLabel: label.bantuan,
        drawerIcon: <IconDrawer name="ic_drawer_bantuan" />,
      },
    },
    drawerPendeta: {
      screen: stackPendeta,
      navigationOptions: {
        drawerLabel: label.pendeta,
        drawerIcon: <IconDrawer name="ic_drawer_pendeta" />,
      },
    },
    drawerGereja: {
      screen: stackGereja,
      navigationOptions: {
        drawerLabel: label.gereja,
        drawerIcon: <IconDrawer name="ic_drawer_gereja" />,
      },
    },
    drawerInstitusi: {
      screen: stackInstitusi,
      navigationOptions: {
        drawerLabel: label.institusi,
        drawerIcon: <IconDrawer name="ic_drawer_institusi" />,
      },
    },
    drawerRepositori: {
      screen: stackRepositori,
      navigationOptions: {
        drawerLabel: label.repositori,
        drawerIcon: <IconDrawer name="ic_folder" />,
      },
    },
    drawerStatistik: {
      screen: stackStatistik,
      navigationOptions: {
        drawerLabel: label.statistik,
        drawerIcon: <IconDrawer name="ic_drawer_statistik" />,
      },
    },
  },
  {
    contentComponent: CustomDrawer,
    drawerWidth: Dimensions.get('window').width * 0.8,
    initialRouteName: 'drawerBeranda',
    unmountInactiveRoutes: true,
  },
);

export {drawerNavigator};
