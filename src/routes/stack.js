import {createStackNavigator} from 'react-navigation-stack';
import warna from 'gsja/src/assets/colors';

// Belajar coba-coba hal baru
import Belajar from '../screens/belajar';

// Daftar Halaman yang digunakan
// isi Profil
import Login from '../screens/login';
import PilihAkses from '../screens/pilihAkses';
import DetailProfil from '../screens/profil/pendeta/detail';
import EditProfil from '../screens/profil/pendeta/edit';
import DetailProfilGereja from '../screens/profil/gereja/detail';
import PilihGereja from '../screens/profil/gereja/pilihGereja';
import EditProfilGereja from '../screens/profil/gereja/edit';
import KartuNama from '../screens/profil/kartuNama';

// Main Content
// List, Detail, dan Add
import ListBeranda from '../screens/beranda/list';
import DetailKegiatan from '../screens/beranda/detail/kegiatan';
import DetailPostingan from '../screens/beranda/detail/postingan';
import AddPostingan from '../screens/beranda/add/postingan';
import ListGereja from '../screens/gereja/list';
import DetailGereja from '../screens/gereja/detail';
import ListInstitusi from '../screens/institusi/list';
import DetailInstitusi from '../screens/institusi/detail';
import ListPendeta from '../screens/pendeta/list';
import DetailPendeta from '../screens/pendeta/detail';
import ListBantuan from '../screens/bantuan/list';
import DetailBantuan from '../screens/bantuan/detail';

// Data-data
import Notifikasi from '../screens/notifikasi';
import Kalender from '../screens/kalender';
import Repositori from '../screens/repositori';
import Statistik from '../screens/statistik';

// Default configurasi untuk header stack navigator
const stackConfig = {
  headerStyle: {
    backgroundColor: warna.merahHeader,
    elevation: 1,
  },
  headerTintColor: warna.putihAsli,
  headerTitleStyle: {
    fontFamily: 'Roboto-Regular',
    fontSize: 22,
    marginLeft: 0,
  },
};

// Stack Auth - Terpisah dari Konten Utama Aplikasi
const stackAuth = createStackNavigator(
  {Login, PilihAkses},
  {headerMode: 'none'},
);

// Stack Kartu Nama
const stackKartuNama = createStackNavigator(
  {KartuNama},
  {defaultNavigationOptions: stackConfig},
);

// Stack Edit Profil
const stackEditProfil = createStackNavigator(
  {DetailProfil, EditProfil},
  {defaultNavigationOptions: stackConfig},
);

// Stack Edit Gereja
const stackEditGereja = createStackNavigator(
  {PilihGereja, DetailProfilGereja, EditProfilGereja},
  {defaultNavigationOptions: stackConfig},
);

// Stack Beranda
const stackBeranda = createStackNavigator(
  {ListBeranda, DetailKegiatan, DetailPostingan, AddPostingan, Notifikasi},
  {defaultNavigationOptions: stackConfig},
);

// Stack Kalender
const stackKalender = createStackNavigator(
  {Kalender},
  {defaultNavigationOptions: stackConfig},
);

// Stack Bantuan
const stackBantuan = createStackNavigator(
  {ListBantuan, DetailBantuan},
  {defaultNavigationOptions: stackConfig},
);

// Stack Pendeta
const stackPendeta = createStackNavigator(
  {ListPendeta, DetailPendeta},
  {defaultNavigationOptions: stackConfig},
);

// Stack Gereja
const stackGereja = createStackNavigator(
  {ListGereja, DetailGereja},
  {defaultNavigationOptions: stackConfig},
);

// Stack Institusi
const stackInstitusi = createStackNavigator(
  {ListInstitusi, DetailInstitusi},
  {defaultNavigationOptions: stackConfig},
);

// Stack Repositori
const stackRepositori = createStackNavigator(
  {Repositori},
  {defaultNavigationOptions: stackConfig},
);

// Stack Statistik
const stackStatistik = createStackNavigator(
  {Statistik},
  {defaultNavigationOptions: stackConfig},
);

export {
  Belajar,
  stackAuth,
  stackKartuNama,
  stackEditProfil,
  stackEditGereja,
  stackBeranda,
  stackKalender,
  stackBantuan,
  stackPendeta,
  stackGereja,
  stackInstitusi,
  stackRepositori,
  stackStatistik,
};
