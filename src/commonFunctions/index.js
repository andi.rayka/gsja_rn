import {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';

// Set Data ke AsyncStorage
// -> Key AsyncStorage (harus unique) dan value yang disimpan
export const setAsyncStorage = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
    // console.log('key', key);
    // console.log('val:', value);
  } catch (error) {
    console.log(error);
  }
};

// Ambil Async Storage Data
// -> Key AsyncStorage yang mau diambil
// -> true/false apkah data perlu di-JSON.parse
export const GetAsyncStorage = (keyName, parseData) => {
  const [data, setData] = useState(null);

  useEffect(() => {
    const ambilData = async () => {
      let hasil = await AsyncStorage.getItem(keyName);

      if (parseData) {
        setData(JSON.parse(hasil));
      } else {
        setData(hasil);
      }
    };

    ambilData();
  }, [keyName, parseData]);

  return data;
};

export const clearAsyncStorage = async () => {
  try {
    await AsyncStorage.clear();
  } catch (error) {
    alert('Gagal logout');
  }
};
