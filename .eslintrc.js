module.exports = {
  root: true,
  extends: '@react-native-community',
  // extends: ['@react-native-community', 'prettier'],
  // plugins: ['prettier'],
  rules: {
    'no-alert': 'off',
    'react-native/no-inline-styles': 'off',
  },
};
