// React Navigation
import {createAppContainer, createSwitchNavigator} from 'react-navigation';

// Halaman yang digunakan
import {stackAuth, Belajar} from './src/routes/stack';
import {drawerNavigator} from './src/routes/drawer';

// Function
import {useScreens} from 'react-native-screens';
import moment from 'moment';
import localTime from 'moment/locale/id';

// useScreens untuk mengoptimalkan performa aplikasi
// Dijelaskan di dokumentasi react-native-screens
// useScreens();

// Buat moment js agar bahasa indonesia
moment.updateLocale('id', localTime);

// Switch Navigator sebagai navigator teratas
const switchNavigator = createSwitchNavigator({
  // Belajar,
  stackAuth,
  drawerNavigator,
});

export default createAppContainer(switchNavigator);
